<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/5/5
  Time: 18:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html class="x-admin-sm">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body>
        <div class="layui-container">
            <div class="fly-panel">
                <div class="fly-none">
                    <h2><i class="layui-icon layui-icon-404"></i></h2>
                    <p>页面或者数据被<a href=""> 纸飞机 </a>运到火星了，啥都看不到了…</p>
                </div>
            </div>
        </div>
        <a href="${pageContext.request.contextPath}/index">点击返回</a>
        <script>

        </script>
    </body>
</html>
