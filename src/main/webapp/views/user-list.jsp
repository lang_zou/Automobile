<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/15
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body class="p20">

        <a href="${pageContext.request.contextPath}/views/user-add.jsp" class="layui-btn">
            <i class="layui-icon"></i>添加用户</a>
        <table class="layui-table layui-form">
            <colgroup>
                <col width="80">
                <col width="80">
                <col width="80">
                <col width="80">
                <col width="80">
                <col width="80">
                <col>
                <col width="150">
            </colgroup>
            <thead>
            <tr>
                <th>序号</th>
                <th>用户名称</th>
                <th>登录名称</th>
                <th>登录密码</th>
                <th>创建日期</th>
                <%--                <th>开关</th>--%>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

        <form hidden action="${pageContext.request.contextPath}/user/" method="post" id="form1">
            <input id="input1" type="text" name="userName">
            <input id="input2" type="text" name="loginName">
            <input id="input3" type="text" name="userPwd">
        </form>

        <script>
            /**
             * 初始化函数
             **/
            $(function () {
                /**
                 * 删除
                 **/
                del();

                /**
                 * 更新
                 **/
                update();

                /**
                 * 查询所有
                 */
                queryAll();
            });

            /**
             * 查询所有
             */
            function queryAll() {

                //异步请求
                $.ajax({
                    url: "${pageContext.request.contextPath}/user/",
                    type: "get",
                    contentType: 'application/json;charset=utf-8',
                    dataType: "json",
                    success: function (msg) {
                        // alert(666)
                        linkTable(msg);
                    }
                });

                function linkTable(msg) {
                    function timestampToTime(timestamp) {
                        var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
                        var Y = date.getFullYear() + '-';
                        var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
                        var D = date.getDate() + ' ';
                        var h = date.getHours() + ':';
                        var m = date.getMinutes() + ':';
                        var s = date.getSeconds();
                        return Y + M + D;
                    }

                    let str = "";
                    for (let i = 0; i < msg.length; i++) {
                        str += "<tr>\n" +
                            "  <td>" + (i + 1) + "</td>\n" +
                            "  <td>" + msg[i].userName + "</td>\n" +
                            "  <td>" + msg[i].loginName + "</td>\n" +
                            "  <td>" + msg[i].userPwd + "</td>\n" +
                            "  <td>" + timestampToTime(msg[i].createDate) + "</td>\n" +
                            "  <td>" +
                            "         <a class=\"upd\" data-href=\"${pageContext.request.contextPath}/user/jump/" + msg[i].userId + "\">修改</a> |\n" +
                            "         <a class=\"del\" data-id=\"" + msg[i].userId + "\" " +
                            "data-href=\"${pageContext.request.contextPath}/user/" + msg[i].userId + "\">删除</a>\n" +
                            "     </td>\n" +
                            " </tr>";
                    }

                    $("tbody").html(str);

                }
            }

            /**
             * 更新
             **/
            function update() {

                $("tbody").on("click", ".upd", function () {

                    var url = $(this).attr("data-href");

                    $("#input1").val($(this).parent().prev().prev().prev().prev().text());
                    $("#input2").val($(this).parent().prev().prev().prev().text());
                    $("#input3").val($(this).parent().prev().prev().text());

                    $("#form1").attr("action", url).submit();

                });
            }

            /**
             * 删除
             **/
            function del() {

                $("tbody").on("click", ".del", function () {

                    var url = $(this).attr("data-href");

                    if (confirm("你确定要删除吗？")) {

                        //异步请求
                        $.ajax({
                            url: url,
                            type: "delete",
                            contentType: 'application/json;charset=utf-8',
                            dataType: "text",
                            success: function (msg) {
                                // alert(msg);
                                queryAll();
                            }
                        });
                    }

                });

            }
        </script>


        <script>
            layui.use('form', function () {
                var form = layui.form, layer = layui.layer, $ = layui.jquery;

                form.on('switch(state)', function (data) {
                    var id = $(data.elem).attr("data-id");
                    $.ajax({
                        url: "/admin/category/state/type/state.html",
                        data: {
                            id: id,
                        },
                        type: "get",
                        dataType: "json",
                        success: function (e) {
                            if (e.code == 1) {
                                layer.msg(e.msg, {
                                    icon: 1
                                });
                            } else {
                                layer.msg(e.msg, {
                                    icon: 2,
                                    shade: 0.5,
                                    time: 2000,
                                    shadeClose: true
                                });
                            }
                        }
                    });
                });

            });
        </script>
        <%--        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/config.js"></script>--%>

    </body>
</html>

