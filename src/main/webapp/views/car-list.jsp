<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/20
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>

    <body class="p20">
        <li class="layui-bg-black" style="height: 40px;font-size: 20px">
            汽车档案
        </li>
        <br>

        <a href="${pageContext.request.contextPath}/views-add/car-add.jsp" class="layui-btn layui-anim layui-anim-up"
           style="height: 35px">
            <i class="layui-icon "></i>新增</a>

        <table class=" layui-table layui-form " lay-filter="table1">
            <colgroup>
                <col width="80">
                <col width="80">
                <col width="100">
                <col width="80">
                <col width="80">

            </colgroup>
            <thead>
            <tr>
                <th>序号</th>
                <th>汽车品牌</th>
                <th>车牌号</th>
                <th>客户姓名</th>
                <%--                <th>开关</th>--%>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>


            </tbody>
        </table>

        <%--        <table class="layui-table"--%>
        <%--               lay-data="{height:315, url:'${pageContext.request.contextPath}/car/2', page:true, id:'test'}"--%>
        <%--               lay-filter="test">--%>
        <%--            <thead>--%>
        <%--            <tr>--%>
        <%--                <th lay-data="{field:'id', width:80, sort: true}">ID</th>--%>
        <%--                <th lay-data="{field:'carBrand', width:240}">汽车品牌</th>--%>
        <%--                <th lay-data="{field:'carNumber', width:240, sort: true}">车牌号</th>--%>
        <%--                <th lay-data="{field:'clientName',width:240,sort:true}">客户姓名</th>--%>

        <%--            </tr>--%>
        <%--            </thead>--%>
        <%--        </table>--%>
        <form hidden action="" method="post" id="form1">
            <label for="input1"></label><input id="input1" type="text" name="carBrand">
            <label for="input2"></label><input id="input2" type="text" name="carNumber">
            <label for="input3"></label><input id="input3" type="text" name="clientName">
        </form>

        <script>
            /**
             * 初始化函数
             **/
            $(function () {
                /**
                 * 删除
                 */
                del();

                /**
                 * 更新
                 */
                update();

                /**
                 * 查询所有
                 */
                queryAll();

                /**
                 *删除
                 */
                function del() {

                    //当删除按钮点击时
                    $("tbody").on("click", ".del", function () {

                        //获取地址
                        var url = $(this).attr("data-href");

                        //判断
                        layer.confirm('你确定要删除吗？', function (index) {

                            //关闭窗口
                            layer.close(index);

                            //异步请求
                            $.ajax({
                                url: url,
                                type: "delete",
                                contentType: 'application/json;charset=utf-8',
                                dataType: "text",
                                success: function (msg) {
                                    //重新加载页面
                                    queryAll();
                                }
                            });
                        });

                    });
                }

                /**
                 * 查询所有
                 */
                function queryAll() {

                    //异步请求
                    $.ajax({
                        url: "${pageContext.request.contextPath}/car/",
                        type: "get",
                        contentType: 'application/json;charset=utf-8',
                        dataType: "json",
                        success: function (msg) {

                            //拼接页面
                            linkTable(msg);
                        }
                    });

                    /**
                     * 拼接页面
                     **/
                    function linkTable(msg) {

                        /**
                         * 车牌号处理（点）
                         */
                        function numDeal(str) {

                            //定义变量接收车牌号前两位
                            var result = str.substring(0, 2);

                            //拼接车牌号
                            result += "·" + str.substring(2, 7);

                            //返回结果
                            return result;
                        }

                        //定义一个字符串
                        let str = "";

                        //循环遍历拼接表格
                        for (let i = 0; i < msg.length; i++) {
                            str += "<tr>\n" +
                                "  <td>" + (i + 1) + "</td>\n" +
                                "  <td>" + msg[i].carBrand + "</td>\n" +
                                "  <td>" + numDeal(msg[i].carNumber) + "" +
                                "  </td>\n" +
                                "  <td>" + msg[i].clientInfo.clientName + " </td>\n" +
                                "  <td>" +
                                "<input hidden  type=\"text\" value=\"" + msg[i].carNumber + "\">" +
                                "         <a class=\"upd\" data-href=\"${pageContext.request.contextPath}/car/jump/" + msg[i].carId + "\">修改</a> |\n"
                                +
                                "         <a class=\"del\" data-id=\"" + msg[i].carId + "\" " +

                                "data-href=\"${pageContext.request.contextPath}/car/" + msg[i].carId + "\">删除</a>\n" +
                                "     </td>\n" +
                                " </tr>";
                        }

                        //替换网页内容
                        $("tbody").html(str);

                    }
                }
            });

            /**
             * 更新
             **/
            function update() {

                //当更新按钮点击时
                $("tbody").on("click", ".upd", function () {

                    //获取地址
                    var url = $(this).attr("data-href");

                    //往隐藏控件中放入值
                    $("#input1").val($(this).parent().prev().prev().prev().text());
                    $("#input2").val($(this).prev().val());
                    $("#input3").val($(this).parent().prev().text());

                    //提交表单
                    $("#form1").attr("action", url).submit();

                });
            }

        </script>
    </body>
</html>
