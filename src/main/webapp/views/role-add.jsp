<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/15
  Time: 20:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body class="p20">


        <fieldset class="layui-elem-field layui-field-title">
            <legend>添加角色</legend>
        </fieldset>


        <form id="form1" class="layui-form " method="post" action="${pageContext.request.contextPath}/role/">
            <div class="layui-form-item">
                <label class="layui-form-label">角色名称</label>
                <div class="layui-input-block">
                    <input type="text" name="roleName" required="" lay-verify="required" placeholder=""
                           autocomplete="off"
                           class="layui-input" value="${role.roleName}">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">角色描述</label>
                <div class="layui-input-block">
                    <input id="input1" type="text" name="roleDesc" required="" lay-verify="required"
                           placeholder="请不要有空格"
                           autocomplete="off" class="layui-input" value="${role.roleDesc}">
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="submit">立即提交</button>
                    <button class="layui-btn layui-btn-primary"
                            onclick="history.back(-1);">返回
                    </button>
                </div>
            </div>
        </form>

        <script>
            $(function () {
                <%--alert(${role.roleName});--%>
                if ($("#input1").val() == "") {
                    $("#form1").attr("action", "${pageContext.request.contextPath}/role/");
                } else {
                    $("#form1").attr("action", "${pageContext.request.contextPath}/role/" +${role.roleId});
                }
            });
        </script>

        <%--        <script src="./static/admin/js/config.js"></script>--%>

    </body>
</html>
