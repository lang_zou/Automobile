<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/26
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body>

        <div class="p20">
            <li class="layui-bg-black" style="height: 40px;font-size: 20px">
                配件采购信息
            </li>
            <br>
            <table id="demo" lay-filter="test"></table>
        </div>

        <script>

            layui.use('table', function () {
                var table = layui.table;
                var form = layui.form;

                table.render({
                    elem: '#demo'
                    , height: 'full-150'
                    , cellMinWidth: 80
                    , url: '${pageContext.request.contextPath}/parts/pur' //数据接口
                    , page: true //开启分页
                    , toolbar: '#toolbarDemo' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
                    , defaultToolbar: ['filter', 'exports', 'print', {
                        title: '爱心',
                        layEvent: 'love',
                        icon: 'layui-icon-heart'
                    }]
                    , cols: [[ //表头
                        {field: 'purchaseId', title: 'ID', width: 150, sort: true, align: "center"}
                        , {
                            field: 'purchaseNo', title: '采购编号', width: 150, sort: true, align: "center",
                            templet: function (pur) {
                                var html = "";
                                if (pur.purchaseNo < 10) {
                                    html += '<div> <span class="layui-bg-green">PC00' + pur.purchaseNo + '</span> </div>';
                                } else if (pur.purchaseNo < 100) {
                                    html += '<div> <span class="">PC0' + pur.purchaseNo + '</span> </div>';
                                } else {
                                    html += '<div> <span class="">PC' + pur.purchaseNo + '</span> </div>';
                                }
                                return html;
                            }
                        }
                        , {field: 'partsType', title: '配件类型', width: 150, align: "center"}
                        , {field: 'partsName', title: '配件名称', width: 150, sort: true, align: "center"}
                        , {field: 'purchaseCount', title: '采购数量', width: 150, sort: true, align: "center"}
                        , {field: 'purchaseSumMoney', title: '采购总金额（元）', width: 150, sort: true, align: "center"}
                        , {
                            field: 'purchaseDate',
                            title: '采购日期',
                            width: 340,
                            sort: true,
                            templet: "<div>{{layui.util.toDateString(d.purchaseDate)}}</div>"
                        }

                        , {align: 'center', toolbar: '#barDemo', fixed: 'right'}
                    ]]
                    , parseData: function (res) { //将原始数据解析成 table 组件所规定的数据
                        if (this.page.curr) {
                            result = res.data.slice(this.limit * (this.page.curr - 1), this.limit * this.page.curr);
                        } else {
                            result = res.data.slice(0, this.limit);
                        }
                        return {"code": res.code, "msg": res.msg, "count": res.count, "data": result};
                    }

                });
                //触发事件
                table.on('toolbar(test)', function (obj) {

                    // var checkStatus = table.checkStatus(obj.config.id);
                    switch (obj.event) {
                        case 'add':
                            // layer.msg("no");
                            openMy('啦啦啦', '${pageContext.request.contextPath}/views-add/parts-buy-add.jsp');
                            break;
                        case 'love':
                            layer.msg('今天也要元气满满喔！');
                            break;
                    }
                });
                //监听行工具事件
                table.on('tool(test)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
                    var data = obj.data //获得当前行数据
                        , layEvent = obj.event; //获得 lay-event 对应的值

                });

            });


        </script>

        <%--右侧操作--%>
        <script type="text/html" id="barDemo">
            <a class="layui-btn layui-btn-primary layui-btn-xs layui-anim layui-anim-rotate" lay-event="none">啦啦啦</a>
        </script>

        <%--        工具栏模板：--%>
        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-sm layui-anim layui-anim-rotate " lay-event="add">
                    新增采购
                </button>
            </div>
        </script>
        <SCRIPT src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/myjs.js"></SCRIPT>
    </body>
</html>
