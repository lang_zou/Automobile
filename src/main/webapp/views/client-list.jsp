<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/20
  Time: 10:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body>

        <div class="p20">
            <li class="layui-bg-black" style="height: 40px;font-size: 20px">
                客户信息
            </li>
            <br>
            <table id="demo" lay-filter="test"></table>
        </div>

        <form hidden action="${pageContext.request.contextPath}/client/jump/" method="post" id="form1"
              class="layui-form" lay-filter="hidForm">

            <input id="input1" type="text" name="clientName">
            <input id="input2" type="text" name="sex">
            <input id="input3" type="text" name="phone">
            <input id="input4" type="text" name="clientId">
        </form>
        <script>

            /**
             * layui使用表格
             */
            layui.use('table', function () {
                var table = layui.table;
                var form = layui.form;

                /**
                 * 表格渲染
                 */
                table.render({
                    elem: '#demo'
                    , height: 'full-150'
                    , url: '${pageContext.request.contextPath}/client/' //数据接口
                    , page: true //开启分页
                    , toolbar: '#toolbarDemo' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
                    , defaultToolbar: ['filter', 'exports', 'print', {
                        title: '爱心',
                        layEvent: 'love',
                        icon: 'layui-icon-heart'
                    }]
                    , cols: [[ //表头
                        {field: 'clientId', title: 'ID', width: 160, sort: true}
                        , {field: 'clientName', title: '客户名称', width: 240, sort: true}
                        , {field: 'sex', title: '性别', width: 160, sort: true}
                        , {field: 'phone', title: '手机号', width: 300}
                        , {
                            field: 'createDate',
                            title: '创建日期',
                            width: 340,
                            sort: true,
                            templet: "<div>{{layui.util.toDateString(d.createDate)}}</div>"
                        }
                        , {width: 200, align: 'center', toolbar: '#barDemo', fixed: 'right'}
                    ]]
                    , parseData: function (res) { //将原始数据解析成 table 组件所规定的数据
                        if (this.page.curr) {
                            result = res.data.slice(this.limit * (this.page.curr - 1), this.limit * this.page.curr);
                        } else {
                            result = res.data.slice(0, this.limit);
                        }
                        return {"code": res.code, "msg": res.msg, "count": res.count, "data": result};
                    }

                });

                //触发事件
                table.on('toolbar(test)', function (obj) {
                    var checkStatus = table.checkStatus(obj.config.id);
                    switch (obj.event) {
                        case 'add':
                            openMy('啦啦啦', '${pageContext.request.contextPath}/views/client-add.jsp');
                            // layer.msg("no");
                            break;
                        case 'love':
                            layer.msg('今天也要元气满满喔！');
                            break;
                    }
                });

                /**
                 * 监听行工具事件
                 */
                table.on('tool(test)', function (obj) {

                    //获得当前行数据
                    var data = obj.data
                        , layEvent = obj.event;

                    //删除时
                    if (layEvent === 'del') {

                        //删除确认
                        layer.confirm('你确定要删除吗?', function (index) {

                            //删除对应行（tr）的DOM结构
                            obj.del();

                            //关闭弹窗
                            layer.close(index);

                            //异步请求
                            $.ajax({
                                url: "${pageContext.request.contextPath}/client/" + data.clientId,
                                type: "delete",
                                contentType: 'application/json;charset=utf-8',
                                dataType: "text",
                                success: function (msg) {
                                }
                            });
                        });

                        //更新
                    } else if (layEvent === 'upd') {

                        // console.log(JSON.stringify(data));
                        //给表单赋值
                        form.val("hidForm", {
                            "clientName": data.clientName
                            , "sex": data.sex
                            , "phone": data.phone
                            , "clientId": data.clientId
                        });

                        //获取表单区域所有值
                        var data1 = form.val("hidForm");

                        // console.log(data1);
                        //提交表单
                        $("#form1").submit();
                    }

                });

            });


        </script>

        <%--右侧操作--%>
        <script type="text/html" id="barDemo">
            <a class="layui-btn layui-btn-primary layui-btn-xs layui-anim layui-anim-rotate" lay-event="upd">修改</a>
            <a class="layui-btn layui-btn-xs layui-bg-black layui-anim layui-anim-rotate" lay-event="del">删除</a>
        </script>

        <%--        工具栏模板：--%>
        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-sm layui-anim layui-anim-rotate " lay-event="add">
                    添加
                </button>
            </div>
        </script>
        <SCRIPT src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/myjs.js"></SCRIPT>
    </body>
</html>
