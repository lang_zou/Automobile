<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/22
  Time: 8:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body>

        <fieldset class="layui-elem-field layui-field-title">
            <legend id="hh">添加客户信息</legend>
        </fieldset>

        <form class="layui-form" action="">

            <%--          存放表单提交方式  --%>
            <input hidden id="hidInp" value="${pageContext.request.contextPath}/client/">

            <div class="layui-form-item">
                <label class="layui-form-label">客户姓名</label>
                <div class="layui-input-block">
                    <input id="input1" type="text" name="clientName" required lay-verify="required" placeholder="请输入"
                           autocomplete="off" class="layui-input" value="${client.clientName}">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">单选框</label>
                <div class="layui-input-block">
                    <input id="man" type="radio" name="sex" value="男" title="男" checked>
                    <input id="woman" type="radio" name="sex" value="女" title="女">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">手机号</label>
                <div class="layui-input-block">
                    <input type="text" name="phone" required lay-verify="required|tellphone" placeholder="请输入"
                           autocomplete="off" class="layui-input" value="${client.phone}">
                </div>
            </div>


            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    <button class="layui-btn layui-btn-primary"
                            onclick="closeMy()">返回
                    </button>

                </div>
            </div>
        </form>

        <script>
            /**
             * 初始化函数
             */
            $(function () {

                //改变表单提交地址
                change();

                //    男，女选择
                if (${client.sex=="女"}) {
                    $("#woman").attr("checked", true);
                }

            });


            /**
             * 改变提交方式
             */
            function change() {
                if ($("#input1").val() !== "") {
                    $("#hh").text("修改客户信息");
                    $("#hidInp").val("${pageContext.request.contextPath}/client/${client.clientId}");
                }
            }
        </script>
        <script>

            /**
             * layui表单
             */
            layui.use('form', function () {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function (data) {
                    //异步请求
                    $.ajax({
                        url: $("#hidInp").val(),
                        type: "post",
                        data: JSON.stringify(data.field),
                        contentType: 'application/json;charset=utf-8',
                        dataType: "text",
                        success: function (msg) {
                            //关闭窗口
                            closeMy();
                        }
                    });
                });

                //自定义表单验证
                var mobile = /^1[3|4|5|7|8]\d{9}$/, phone = /^0\d{2,3}-?\d{7,8}$/;

                /**
                 * 表单验证
                 */
                form.verify({
                    tellphone: function (value) {

                        //测试
                        var flag = mobile.test(value) || phone.test(value);

                        //如果错误给出提示
                        if (!flag) {
                            return '请输入正确座机号码或手机号';
                        }
                    }
                });
            });
        </script>
        <SCRIPT src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/myjs.js"></SCRIPT>
    </body>
</html>
