<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/19
  Time: 19:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body class="p20">
        <li class="layui-bg-black layui-anim layui-anim-up" style="height: 40px;font-size: 20px">
            库存管理
        </li>
        <br>
        <table id="demo" lay-filter="test"></table>

        <form hidden action="${pageContext.request.contextPath}/client/jump/" method="post" id="form1"
              class="layui-form" lay-filter="hidForm">

            <input id="input1" type="text" name="clientName">
            <input id="input2" type="text" name="sex">
            <input id="input3" type="text" name="phone">
            <input id="input4" type="text" name="clientId">
        </form>
        <script>
            function openModak() {
                $("[name='testname']").val("xxxxxxxxxxxxxxx");//向模态框中赋值
                layui.use(['layer'], function () {
                    var layer = layui.layer, $ = layui.$;
                    layer.open({
                        type: 1,//类型
                        area: ['700px', '300px'],//定义宽和高
                        title: '修改出售价格',//题目
                        shadeClose: false,//点击遮罩层关闭
                        content: $('#motaikunag')//打开的内容
                    });
                })
            }

            layui.use('table', function () {
                var table = layui.table;
                var form = layui.form;

                table.render({
                    elem: '#demo'
                    , height: 'full-150'
                    , url: '${pageContext.request.contextPath}/parts/rep' //数据接口
                    , page: true //开启分页
                    , toolbar: '#toolbarDemo' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
                    , defaultToolbar: ['filter', 'exports', 'print', {
                        title: '爱心',
                        layEvent: 'love',
                        icon: 'layui-icon-heart'
                    }]
                    , cols: [[ //表头
                        {field: 'partsId', title: 'ID', width: 160, sort: true}
                        , {field: 'partsType', title: '配件类型', width: 240, sort: true}
                        , {field: 'partsName', title: '配件名称', width: 240, sort: true}
                        , {field: 'partsPrice', title: '配件出售价（元）', width: 300}
                        , {field: 'count', title: '库存数量', width: 300}

                        , {width: 200, align: 'center', toolbar: '#barDemo', fixed: 'right'}
                    ]]
                    , parseData: function (res) { //将原始数据解析成 table 组件所规定的数据
                        if (this.page.curr) {
                            result = res.data.slice(this.limit * (this.page.curr - 1), this.limit * this.page.curr);
                        } else {
                            result = res.data.slice(0, this.limit);
                        }
                        return {"code": res.code, "msg": res.msg, "count": res.count, "data": result};
                    }

                });
                //触发事件
                table.on('toolbar(test)', function (obj) {
                    var checkStatus = table.checkStatus(obj.config.id);
                    switch (obj.event) {
                        case 'add':
                        <%--openMy('啦啦啦', '${pageContext.request.contextPath}/views/client-add.jsp');--%>
                            layer.msg("啦啦啦");
                            break;
                        case 'love':
                            layer.msg('今天也要元气满满喔！');
                            break;
                    }
                });
                //监听行工具事件
                table.on('tool(test)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
                    var data = obj.data //获得当前行数据
                        , layEvent = obj.event; //获得 lay-event 对应的值

                    if (layEvent === 'del') {
                        layer.confirm('你确定要删除吗?', function (index) {
                            obj.del(); //删除对应行（tr）的DOM结构
                            layer.close(index);
                            //异步请求
                            $.ajax({
                                url: "${pageContext.request.contextPath}/parts/rep" + data.partsId,
                                type: "delete",
                                contentType: 'application/json;charset=utf-8',
                                dataType: "text",
                                success: function (msg) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else if (layEvent === 'upd') {
                        // layer.msg('编辑操作，当前行' + JSON.stringify(data));
                        // console.log(JSON.stringify(data));

                        //异步请求
                        $.ajax({
                            url: "${pageContext.request.contextPath}/parts/rep/",
                            type: "post",
                            data: JSON.stringify(data),
                            contentType: 'application/json;charset=utf-8',
                            dataType: "json",
                            success: function (msg) {
                                // layer.msg(msg.maintainInfo);
                                sessionStorage.setItem('parts', JSON.stringify(msg));
                                // console.log(msg);
                                $("#PriceForm").attr("action", "${pageContext.request.contextPath}/parts/rep/" + msg.partsId);
                                $("#price").val(msg.partsPrice);
                                openModak();
                                <%--openMy('啦啦啦', ' ${pageContext.request.contextPath}/views-add/rep-add.jsp');--%>
                            }
                        });

                    }

                });

            });
        </script>
        <script>
            /**
             * 初始化函数
             **/
            $(function () {

            });

        </script>


        <%--右侧操作--%>
        <script type="text/html" id="barDemo">
            <a class="layui-btn layui-bg-blue layui-btn-xs layui-anim layui-anim-rotate"
               lay-event="upd">修改</a>
            <%--            <a class="layui-btn layui-btn-xs layui-bg-black layui-anim layui-anim-rotate" lay-event="del">删除</a>--%>
        </script>

        <%--        工具栏模板：--%>
        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-sm layui-anim layui-anim-rotate " lay-event="add">
                    啦啦啦
                </button>
            </div>
        </script>

        <SCRIPT src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/myjs.js"></SCRIPT>

    </body>
    <!--模仿bootstrap的模态框-->
    <div id="motaikunag" style="display: none;">
        <form id="PriceForm" class="layui-form" action="" method="post">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <br>
                    <br>
                    <%--                你的内容 9/12--%>
                </div>
                <div class="layui-col-md3">
                    <%--                你的内容 3/12--%>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">配件售卖价</label>
                <div class="layui-input-block">
                    <input id="price" type="number" name="partsPrice" required lay-verify="required" placeholder="请输入"
                           autocomplete="off" class="layui-input" value="${parts.partsPrice}">
                </div>
            </div>

            <br/>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                    <button class="layui-btn layui-btn-primary"
                            onclick="window.location('')">
                        返回
                    </button>

                </div>
            </div>
        </form>
    </div>
</html>
