<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/20
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body class="p20">
        <li class="layui-bg-black" style="height: 40px;font-size: 20px">
            配件出库
        </li>
        <br>
        <table id="demo" lay-filter="test"></table>
        </div>

        <%--        <div>--%>
        <%--            <span class="layui-bg-orange">维修中</span>--%>
        <%--        </div>--%>
        <script>

            layui.use('table', function () {
                var table = layui.table;
                var form = layui.form;

                // 渲染表格
                table.render({
                    elem: '#demo'
                    , height: 'full-200'
                    , url: '${pageContext.request.contextPath}/maintain/out' //数据接口
                    , page: true //开启分页
                    , cellMinWidth: 80
                    , toolbar: '#toolbarDemo' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
                    , defaultToolbar: ['filter', 'exports', 'print', {
                        title: '爱心',
                        layEvent: 'love',
                        icon: 'layui-icon-heart'
                    }]
                    , cols: [[ //表头
                        {field: 'id', fixed: 'left', align: 'center', title: 'ID', width: 80, sort: true}
                        , {
                            field: 'maintainNo', align: 'center', title: '维修编号', width: 120, sort: true,
                            templet: function (maintain) {
                                var html = "";
                                if (parseInt(maintain.maintainNo) < 10) {
                                    html += '<div> <span class="layui-bg-green">MT00' + maintain.maintainNo + '</span> </div>';
                                } else if (parseInt(maintain.maintainNo) < 100) {
                                    html += '<div> <span class="layui-bg-green">MT0' + maintain.maintainNo + '</span> </div>';
                                } else {
                                    html += '<div> <span class="layui-bg-green">MT' + maintain.maintainNo + '</span> </div>';
                                }
                                return html;
                            }
                        }
                        , {field: 'maintainInfo', align: 'center', title: '维修详情', width: 240}
                        , {
                            field: 'state', align: 'center',
                            title: '维修状态',
                            width: 120,
                            sort: true,
                            templet: function (maintain) {
                                var html = "";
                                if (maintain.state === 2) {
                                    html += '<div> <span class="layui-bg-orange">配件出库中</span> </div>';
                                } else if (maintain.state === 0) {
                                    html += '<div> <span class="layui-bg-black">维修中</span> </div>';
                                }
                                return html;
                            }
                        }
                        , {
                            field: 'maintainDate',
                            title: '创建日期',
                            width: 240,
                            sort: true,
                            templet: "<div>{{layui.util.toDateString(d.maintainDate)}}</div>"
                        }
                        , {field: 'maintainCost', align: 'center', title: '维修价格（元）', width: 150, sort: true}

                        , {field: 'accendantName', align: 'center', title: '维修人员', width: 120}
                        , {
                            field: 'partsName',
                            title: '使用配件',
                            width: 200,
                            templet: '#partsName'
                        }

                        , {width: 150, align: 'center', toolbar: '#barDemo', fixed: 'right'}
                    ]]
                    , parseData: function (res) { //将原始数据解析成 table 组件所规定的数据
                        if (this.page.curr) {
                            result = res.data.slice(this.limit * (this.page.curr - 1), this.limit * this.page.curr);
                        } else {
                            result = res.data.slice(0, this.limit);
                        }
                        return {"code": res.code, "msg": res.msg, "count": res.count, "data": result};
                    }
                });

                //监听顶部工具事件
                table.on('toolbar(test)', function (obj) {
                    var checkStatus = table.checkStatus(obj.config.id);
                    switch (obj.event) {
                        case 'add':
                        <%--window.location.href = '${pageContext.request.contextPath}/views/client-add.jsp';--%>

                            break;
                        case 'love':
                            layer.msg('今天也要元气满满喔！');
                            break;
                    }
                });

                //监听行工具事件
                table.on('tool(test)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"

                    //获得对应的值
                    var data = obj.data, layEvent = obj.event;

                    if (layEvent === 'del') {

                        layer.confirm('你确定要取消吗?', function (index) {

                            //删除对应行（tr）的DOM结构
                            obj.del();

                            //异步请求
                            $.ajax({
                                url: "${pageContext.request.contextPath}/maintain/cancel/" + data.id,
                                type: "delete",
                                contentType: 'application/json;charset=utf-8',
                                dataType: "text",
                                success: function (msg) {
                                    layer.msg("取消成功……");
                                }
                            });

                            //关闭弹窗
                            layer.close(index);


                        });
                    } else if (layEvent === 'upd') {
                        // layer.msg('编辑操作，当前行' + JSON.stringify(data));
                        layer.confirm('你确定要出库吗?', function (index) {

                            //关闭弹窗
                            layer.close(index);

                            //异步请求
                            $.ajax({
                                url: "${pageContext.request.contextPath}/parts/out/" + data.id,
                                type: "get",
                                contentType: 'application/json;charset=utf-8',
                                dataType: "text",
                                success: function (msg) {
                                    layer.msg("配件成功出库……");

                                    window.location.reload();
                                }
                            });
                        });

                    }

                });

            });


        </script>

        <%--        右侧操作--%>
        <script type="text/html" id="barDemo">
            <a class="layui-btn layui-bg-blue layui-btn-xs layui-anim layui-anim-rotate"
               lay-event="upd">出库</a>
            <a class="layui-btn layui-btn-xs layui-bg-black layui-anim layui-anim-rotate" lay-event="del">取消</a>
        </script>

        <%--        工具栏模板：--%>
        <%--        <script type="text/html" id="toolbarDemo">--%>
        <%--            <div class="layui-btn-container">--%>
        <%--                <button class="layui-btn layui-btn-sm layui-anim layui-anim-rotate " lay-event="add">--%>
        <%--                    新增工单--%>
        <%--                </button>--%>
        <%--            </div>--%>
        <%--        </script>--%>

        <!--输出配件-->
        <script type="text/html" id="partsName">
            {{#
            for(let i=0;i
            <d.parts.length;i++){ }} <span class="layui-badge-dot layui-bg-green">
                    </span>{{d.parts[i].partsName
            }}
            {{#
            }
            }}
        </script>
    </body>
    <SCRIPT src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/myjs.js"></SCRIPT>
</html>
