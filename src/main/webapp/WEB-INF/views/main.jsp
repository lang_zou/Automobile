<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/15
  Time: 11:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>后台首页 - QAdmin后台模板 - iframe版演示</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/test/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body class="p20">

        <h1 style="position: absolute;top: 100px">${loginUser.userName}, 欢迎回来！</h1>

        <div class="container">
            <div class="pole">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="pole">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="pole">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="neon neon_a">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="neon neon_b">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="neon neon_c">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/script.js"></script>

    </body>
</html>
