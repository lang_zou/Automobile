<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/15
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body>

        <div id="app">
            <!--顶栏-->
            <header>
                <h1 v-text="webname" style="font-size: 15px"></h1>
                <div class="breadcrumb">
                    <i class="layui-icon">&#xe715;</i>
                    <ul>
                        <li v-for="vo in address">
                            <a v-text="vo.name" :href="vo.url"></a> <span>/</span>
                        </li>
                    </ul>
                </div>
            </header>

            <div class="main">
                <!--左栏-->
                <div class="left">
                    <ul class="cl">
                        <!--顶级分类-->
                        <li v-for="vo,index in menu" :class="{hidden:vo.hidden}">
                            <a href="javascript:;" :class="{active:vo.active}" @click="onActive(index)">
                                <i class="layui-icon" v-html="vo.icon"></i>
                                <span v-text="vo.name"></span>
                                <i class="layui-icon arrow" v-show="vo.url.length==0">&#xe61a;</i> <i v-show="vo.active"
                                                                                                      class="layui-icon active">&#xe623;</i>
                            </a>
                            <!--子级分类-->
                            <div v-for="vo2,index2 in vo.list">
                                <a :href="vo2.url" target="main" :class="{active:vo2.active}" v-text="vo2.name"></a>
                                <i v-show="vo2.active" class="layui-icon active">&#xe623;</i>
                            </div>
                        </li>
                    </ul>
                </div>

                <!--右侧-->
                <div class="right">
                    <iframe src="${pageContext.request.contextPath}/main" name="main" marginwidth="0" marginheight="0"
                            frameborder="0"
                            scrolling="auto" target="_self"></iframe>
                </div>
            </div>
        </div>

        <script>

            //菜单
            var menu = [
                {
                    "name": "",
                    "icon": "&#xe68e;",
                    "url": "",
                    "hidden": true,
                    "list": [{
                        "name": "首页",
                        "url": "${pageContext.request.contextPath}/main",
                    }]
                }
                <c:forEach var="role" items="${roles}" varStatus="st">
                <c:if test="${ role.roleName == \"管理员\"}">
                , {
                    "name": "系统配置",
                    "icon": "&#xe620;",
                    "url": "",
                    "hidden": true,
                    "list": [{
                        "name": "用户管理",
                        "url": "${pageContext.request.contextPath}/views/user-list.jsp",
                    }, {
                        "name": "角色管理",
                        "url": "${pageContext.request.contextPath}/views/role-list.jsp"
                    }, {
                        "name": "资源管理",
                        "url": "${pageContext.request.contextPath}/views/res-list.jsp"
                    }]
                }
                </c:if>

                <c:if test="${ role.roleName == \"仓库管理员\"}">
                , {
                    "name": "配件管理",
                    "icon": "&#xe653;",
                    "url": "",
                    "hidden": false,
                    "list": [{
                        "name": "配件采购",
                        "url": "${pageContext.request.contextPath}/views/parts-buy.jsp",
                    }, {
                        "name": "配件出库",
                        "url": "${pageContext.request.contextPath}/views/parts-use.jsp"
                    }, {
                        "name": "库存管理",
                        "url": "${pageContext.request.contextPath}/views/parts-manage.jsp"
                    }]
                }
                </c:if>

                <c:if test="${ role.roleName == \"前台接待\"}">
                , {
                    "name": "维修接待",
                    "icon": "&#xe609;",
                    "url": "",
                    "hidden": false,
                    "list": [{
                        "name": "工单处理",
                        "url": "${pageContext.request.contextPath}/views/order-manage.jsp",
                    }, {
                        "name": "历史查询",
                        "url": "${pageContext.request.contextPath}/views/order-list.jsp"
                    }
                        <%--, {--%>
                        <%--    "name": "预约管理",--%>
                        <%--    "url": "${pageContext.request.contextPath}/views/sub-manage.jsp"--%>
                        <%--}--%>
                        , {
                            "name": "汽车档案",
                            "url": "${pageContext.request.contextPath}/views/car-list.jsp"
                        }
                        , {
                            "name": "配件基础数据",
                            "url": "${pageContext.request.contextPath}/views/parts-list.jsp"
                        }
                    ]
                }
                , {
                    "name": "客户关系",
                    "icon": "&#xe609;",
                    "url": "",
                    "hidden": false,
                    "list": [{
                        "name": "客户资料",
                        "url": "${pageContext.request.contextPath}/views/client-list.jsp"
                    }, {
                        "name": "业务统计",
                        "url": "${pageContext.request.contextPath}/views/order-list.jsp"
                    }
                        <%--, {--%>
                        <%--    "name": "客户分析",--%>
                        <%--    "url": "${pageContext.request.contextPath}/views/res-list.jsp"--%>
                    ]

                }
                </c:if>

                <c:if test="${ role.roleName == \"财务人员\"}">
                <%--, {--%>
                <%--    "name": "报表中心",--%>
                <%--    "icon": "&#xe609;",--%>
                <%--    "url": "",--%>
                <%--    "hidden": true,--%>
                <%--    "list": [{--%>
                <%--        "name": "运营报表",--%>
                <%--        "url": "${pageContext.request.contextPath}/views/company-report.jsp",--%>
                <%--    }, {--%>
                <%--        "name": "维修报表",--%>
                <%--        "url": "${pageContext.request.contextPath}/views/maintain-report.jsp"--%>
                <%--    }--%>
                <%--        , {--%>
                <%--            "name": "库房报表",--%>
                <%--            "url": "${pageContext.request.contextPath}/views/repertory-report.jsp"--%>
                <%--        }, {--%>
                <%--            "name": "财务报表",--%>
                <%--            "url": "${pageContext.request.contextPath}/views/account-report.jsp"--%>
                <%--        }--%>

                <%--    ]--%>
                <%--}--%>

                </c:if>

                </c:forEach>

                ,
                {
                    "name":
                        "退出登录",
                    "icon":
                        "&#xe65c;",
                    "url":
                        "${pageContext.request.contextPath}/out",
                    "list":
                        []
                }];

            //渲染侧边栏
            const config = {
                name: "汽车维修管理系统",
                menu: menu
            };

        </script>

        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/script.js"></script>
    </body>
</html>
