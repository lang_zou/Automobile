/**
 * 关闭弹出层
 **/
function closeMy() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.location.reload();
    parent.layer.close(index);
    // parent.
}

/**
 * [open 打开弹出层]
 * @param  {[type]}  title [弹出层标题]
 * @param  {[type]}  url   [弹出层地址]
 * @param  {[type]}  w     [宽]
 * @param  {[type]}  h     [高]
 * @param  {Boolean} full  [全屏]
 * @return {[type]}        [description]
 */
function openMy(title, url, w, h, full) {
    if (title == null || title == '') {
        var title = false;
    }
    ;
    if (url == null || url == '') {
        var url = "404.html";
    }
    ;
    if (w == null || w == '') {
        var w = ($(window).width() * 0.9);
    }
    ;
    if (h == null || h == '') {
        var h = ($(window).height() - 50);
    }
    ;
    var index = layer.open({
        type: 2,
        area: [w + 'px', h + 'px'],
        fix: false, //不固定
        maxmin: true,
        shadeClose: true,
        shade: 0.4,
        title: title,
        content: url
    });
    if (full) {
        layer.full(index);
    }
}

/**
 * 订单编号处理
 * @param maintain
 * @returns {string}
 */
function toMaintainNo(maintain) {
    var html = "";
    if (parseInt(maintain.maintainNo) < 10) {
        html += '<div> <span class="layui-bg-green">MT00' + maintain.maintainNo + '</span> </div>';
    } else if (parseInt(maintain.maintainNo < 100)) {
        html += '<div> <span class="">MT0' + maintain.maintainNo + '</span> </div>';
    } else {
        html += '<div> <span class="">MT' + maintain.maintainNo + '</span> </div>';
    }
    return html;
}