<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="com.zou.service.MaintainService" %>
<%@ page import="com.zou.service.impl.MaintainServiceImpl" %>
<%@ page import="com.zou.service.AccendantService" %>
<%@ page import="com.zou.service.impl.AccendantServiceImpl" %>
<%@ page import="com.zou.bean.Accendant" %>
<%@ page import="java.util.List" %>
<%@ page import="com.zou.service.impl.CarInfoServiceImpl" %>
<%@ page import="com.zou.bean.CarInfo" %>
<%@ page import="com.zou.service.ClientService" %>
<%@ page import="com.zou.service.impl.ClientServiceImpl" %><%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/25
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>

    <style>

    </style>
    <body>

        <fieldset class="layui-elem-field layui-field-title">
            <legend id="hh">新增工单</legend>
        </fieldset>

        <form class="layui-form" action="">

            <%--          存放表单提交方式  --%>
            <input hidden id="hidInp" value="${pageContext.request.contextPath}/maintain/">

            <%--            <div class="layui-form-item">--%>
            <%--                <label class="layui-form-label">维修编号</label>--%>
            <%--                <div class="layui-input-block">--%>
            <%--                    <input type="text" name="maintainNo" required lay-verify="required|No" placeholder="请输入"--%>
            <%--                           autocomplete="off" class="layui-input" value="MT">--%>
            <%--                </div>--%>
            <%--            </div>--%>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">维修详情</label>
                        <div class="layui-input-block">
                    <textarea id="input2" name="maintainInfo" placeholder="请输入内容" required lay-verify="required"
                              class="layui-textarea"></textarea>
                        </div>
                    </div>
                </div>
                <div class="layui-inline">

                    <label class="layui-form-label">维修汽车</label>
                    <div class="layui-input-block">
                        <select name="carId" lay-verify="" lay-search required lay-verify="required"
                                style="height: 200px">
                            <option value="">请选择要维修的汽车</option>

                            <%
                                ApplicationContext app = WebApplicationContextUtils.getWebApplicationContext
                                        (this.getServletConfig().getServletContext());
                                CarInfoServiceImpl carService = app.getBean(CarInfoServiceImpl.class);
                                List<CarInfo> cars = carService.selectAll();
                                app.getBean(ClientServiceImpl.class).selectAll();
                                for (CarInfo car : cars) {
                            %>
                            <option value="<%=car.getCarId()%>"><%=car.getCarNumber()%>
                            </option>
                            <%
                                }
                            %>

                        </select>
                    </div>
                </div>

            </div>

            <div class="layui-form-item">
                <div class="layui-inline">

                    <label class="layui-form-label">配件类别</label>
                    <div class="layui-input-inline">
                        <select name="partsType" lay-verify="" lay-filter="partsSel">
                            <option value="">请选择配件类别</option>
                            <option value="发动机配件">发动机配件</option>
                            <option value="传动系配件">传动系配件</option>
                            <option value="转向系配件">转向系配件</option>
                            <option value="制动系配件">制动系配件</option>
                            <option value="其他">其他</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">使用配件</label>
                <div class="layui-input-inline" id="test1"></div>
            </div>

            <br>
            <br>
            <br>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">维修价格</label>
                    <div class="layui-input-block">
                        <input id="input3" type="number" name="maintainCost" required lay-verify="required"
                               placeholder="请输入"
                               autocomplete="off"
                               class="layui-input" value="">
                    </div>
                </div>
                <div class="layui-inline">

                    <label class="layui-form-label">维修人员</label>
                    <div class="layui-input-block">
                        <select name="accendantId" lay-verify="" lay-search required lay-verify="required">
                            <option value="">请选择维修人员</option>

                            <%

                                //                            MaintainService mService = app.getBean(MaintainServiceImpl.class);
                                AccendantService aService = app.getBean(AccendantServiceImpl.class);
                                List<Accendant> accendants = aService.selectAll();

                                for (Accendant a : accendants) {
                            %>
                            <option value="<%=a.getAccendantId()%>"><%=a.getAccendantName()%>
                            </option>
                            <%
                                }
                            %>

                        </select>
                    </div>
                </div>
            </div>


            <%--            <button id="getb">获取右侧数据</button>--%>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                    <%--                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>--%>
                    <button class="layui-btn layui-btn-primary"
                            onclick="closeMy()">返回
                    </button>

                </div>
            </div>
        </form>

        <script>


            function partsLoad() {
                var it = $("this option:selected").val();
                alert(it);
            }

            layui.use('transfer', function () {
                var transfer = layui.transfer;


            });


            /**
             * 初始化函数
             */
            $(function () {


            });


        </script>
        <script>

            /**
             * layui表单
             */
            layui.use(function () {
                var form = layui.form;
                var transfer = layui.transfer;
                form.on('select(partsSel)', function (data) {
                    // console.log(data.elem); //得到select原始DOM对象
                    // console.log(data.value); //得到被选中的值
                    // console.log(data.othis); //得到美化后的DOM对象

                    //异步请求加载
                    $.ajax({
                        "url": '${pageContext.request.contextPath}/parts/rep/' + data.value,
                        "type": "get",
                        "contentType": 'application/json;charset=utf-8',
                        "dataType": "json",
                        "success": function (msg) {
                            // layer.msg(JSON.stringify(msg));
                            console.log(msg);
                            //渲染
                            transfer.render({
                                elem: '#test1'  //绑定元素
                                , title: ['库存配件', '使用配件']  //自定义标题
                                //,width: 150 //定义宽度
                                , showSearch: true //显示搜索框
                                , height: 210 //定义高度
                                , data: msg //disabled 是否禁用  checked 是否选中
                                , parseData: function (res) {
                                    return {
                                        "value": res.partsId //数据值
                                        , "title": res.partsName //数据标题
                                        , "disabled": !res.count //是否禁用
                                        // , "checked": res.checked //是否选中
                                    }
                                }
                                , id: 'demo1' //定义索引 重新加载reload或者获取右侧数据时可以用到
                            });
                            var getData = transfer.getData('demo1');
                            $("#getb").click(function () {
                                var getData = transfer.getData('demo1');
                            });
                        }
                    });
                });

                //监听提交
                form.on('submit(formDemo)', function (data) {
                    // console.log(transfer.getData('demo1'));
                    // data.field.parts = transfer.getData('demo1');
                    // console.log(JSON.stringify(data.field));
                    //异步请求
                    $.ajax({
                        url: $("#hidInp").val(),
                        type: "post",
                        data: JSON.stringify({
                            maintain: data.field,
                            parts: transfer.getData('demo1')
                        }),
                        contentType: 'application/json;charset=utf-8',
                        dataType: "text",
                        success: function (msg) {
                            closeMy();
                            // alert(66);
                            <%--window.location.href = '${pageContext.request.contextPath}/views/car-list.jsp';--%>
                        }
                    });
                    return false;
                });


            });
        </script>

        <SCRIPT src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/myjs.js"></SCRIPT>
    </body>
</html>
