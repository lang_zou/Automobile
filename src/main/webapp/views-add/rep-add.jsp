<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/25
  Time: 18:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body>

        <fieldset class="layui-elem-field layui-field-title">
            <legend id="hh">新增配件库存</legend>
        </fieldset>

        <form class="layui-form" action="">

            <%--          存放表单提交方式  --%>
            <input hidden id="hidInp" value="${pageContext.request.contextPath}/parts/rep">

            <div class="layui-form-item">
                <label class="layui-form-label">配件名称</label>
                <div class="layui-input-block">
                    <input id="input1" type="text" name="partsName" required lay-verify="required" placeholder="请输入"
                           autocomplete="off" class="layui-input" value="${parts.partsName}">
                </div>
            </div>


            <div class="layui-form-item">
                <label class="layui-form-label">配件类型</label>
                <div class="layui-form-item layui-form-text">
                    <select name="partsType" lay-verify="">
                        <option value="">请选择配件类别</option>
                        <option value="发动机配件">发动机配件</option>
                        <option value="传动系配件">传动系配件</option>
                        <option value="转向系配件">转向系配件</option>
                        <option value="制动系配件">制动系配件</option>
                        <option value="其他">其他</option>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">配件售卖价</label>
                <div class="layui-input-block">
                    <input type="number" name="partsPrice" required lay-verify="required" placeholder="请输入"
                           autocomplete="off" class="layui-input" value="${parts.partsPrice}">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">配件数量</label>
                <div class="layui-input-block">
                    <input type="number" name="count" required lay-verify="required" placeholder="请输入"
                           autocomplete="off" class="layui-input" value="${parts.count}">
                </div>
            </div>


            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    <button class="layui-btn layui-btn-primary"
                            onclick="closeMy()">
                        返回
                    </button>

                </div>
            </div>
        </form>

        <script>
            /**
             * 初始化函数
             */
            $(function () {
                <%--    // alert($("#input1").val());--%>

            });


        </script>
        <script>

            /**
             * layui表单
             */
            layui.use('form', function () {
                var form = layui.form;
                //监听提交
                form.on('submit(formDemo)', function (data) {
                    alert(JSON.stringify(data.field));
                    // alert($("#hidInp").val());
                    //异步请求
                    $.ajax({
                        url: $("#hidInp").val(),
                        type: "post",
                        data: JSON.stringify(data.field),
                        contentType: 'application/json;charset=utf-8',
                        dataType: "text",
                        success: function (msg) {
                            closeMy();
                        }
                    });
                });


            });
        </script>

        <SCRIPT src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/myjs.js"></SCRIPT>
    </body>
</html>
