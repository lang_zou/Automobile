<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="com.zou.service.PurchaseService" %>
<%@ page import="com.zou.service.impl.PurchaseServiceImpl" %>
<%@ page import="com.zou.bean.Purchase" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/26
  Time: 18:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body>

        <fieldset class="layui-elem-field layui-field-title">
            <legend id="hh">新增配件采购</legend>
        </fieldset>

        <form class="layui-form" action="${pageContext.request.contextPath}/parts/pur/add" method="post">

            <%--          存放表单提交方式  --%>
            <input hidden id="hidInp" value="${pageContext.request.contextPath}/parts/pur/">

            <div class="layui-form-item">
                <%--                <div class="layui-inline">--%>
                <%--                    <label class="layui-form-label">配件类别</label>--%>
                <%--                    <div class="layui-input-inline">--%>
                <%--                        <select name="partsType" lay-verify="" lay-filter="partsSel" required lay-verify="required">--%>
                <%--                            <option value="">请选择配件类别</option>--%>
                <%--                            <option value="发动机配件">发动机配件</option>--%>
                <%--                            <option value="传动系配件">传动系配件</option>--%>
                <%--                            <option value="转向系配件">转向系配件</option>--%>
                <%--                            <option value="制动系配件">制动系配件</option>--%>
                <%--                            <option value="其他">其他</option>--%>
                <%--                        </select>--%>
                <%--                    </div>--%>
                <%--                </div>--%>
                <div class="layui-inline">

                    <label class="layui-form-label">配件名称</label>
                    <div class="layui-input-block">
                        <select id="selParts" name="partsInfo" lay-search required
                                lay-verify="required">
                            <option value="">请选择配件名称</option>

                            <%
                                ApplicationContext app = WebApplicationContextUtils.getWebApplicationContext
                                        (this.getServletConfig().getServletContext());

                                PurchaseService pService = app.getBean(PurchaseServiceImpl.class);

                                List<Purchase> parts = pService.selectAllParts();

                                for (Purchase p : parts) {
                            %>
                            <option value="<%=p.getPartsInfoId()%>|<%=p.getPartsType()%>|<%=p.getPartsName()%>|<%=p.getPurchaseSumMoney()%>"><%=p.getPartsName()%>
                            </option>
                            <%
                                }
                            %>

                        </select>
                    </div>
                </div>


            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">采购数量</label>
                <div class="layui-input-block">
                    <input type="number" name="purchaseCount" required lay-verify="required"
                           placeholder="请输入"
                           autocomplete="off"
                           class="layui-input" value="">
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                    <button class="layui-btn layui-btn-primary"
                            onclick="closeMy()">返回
                    </button>

                </div>
            </div>
        </form>

        <script>
            /**
             * 初始化函数
             */
            $(function () {

            });


        </script>
        <script>

            /**
             * layui表单
             */
            layui.use(function () {

                //实例化layui对象
                var form = layui.form;
                var transfer = layui.transfer;

                //监听提交
                form.on('submit(formDemo)', function (data) {
                    console.log(JSON.stringify(data.field));
                    //异步请求
                    // $.ajax({
                    //     url: $("#hidInp").val(),
                    //     type: "post",
                    //     data: JSON.stringify(data.field),
                    //     contentType: 'application/json;charset=utf-8',
                    //     dataType: "text",
                    //     success: function (msg) {
                    //         layer.msg(666);
                    //     }
                    // });
                });

            });
        </script>

        <SCRIPT src="${pageContext.request.contextPath}/qadmin-iframe/static/admin/js/myjs.js"></SCRIPT>

    </body>
</html>
