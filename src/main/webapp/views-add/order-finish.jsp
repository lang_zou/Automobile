<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/25
  Time: 8:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body>

        <fieldset class="layui-elem-field layui-field-title">
            <legend id="hh">完工确认</legend>
        </fieldset>

        <form class="layui-form" action="">

            <%--          存放表单提交方式  --%>
            <input hidden id="hidInp" value="${pageContext.request.contextPath}/maintain/">


            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">维修详情</label>
                <div class="layui-input-block">
                    <textarea id="input2" name="maintainInfo" placeholder="请输入内容"
                              class="layui-textarea">${maintain.maintainInfo}</textarea>
                </div>
            </div>


            <div class="layui-form-item">
                <label class="layui-form-label">维修价格</label>
                <div class="layui-input-block">
                    <input id="input3" type="number" name="maintainCost" required lay-verify="required"
                           placeholder="请输入"
                           autocomplete="off" class="layui-input" value="${maintain.maintainCost}">
                </div>
            </div>


            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">确认完工</button>
                    <button class="layui-btn layui-btn-primary"
                            onclick="closeMy()">返回
                    </button>

                </div>
            </div>
        </form>

        <script>
            /**
             * 关闭弹出层
             **/
            function closeMy() {
                var index = parent.layer.getFrameIndex(window.name);
                parent.layer.close(index);
            }

            /**
             * 初始化函数
             */
            $(function () {
                var ss = sessionStorage.getItem('maintain');

                //解决未定义问题
                ss = eval("(" + ss + ")");
                // alert(eval("(" + ss + ")").maintainNo);

                // $("#input1").val(ss.maintainNo);
                $("#input2").text(ss.maintainInfo);
                $("#input3").val(ss.maintainCost);

                $("#hidInp").val("${pageContext.request.contextPath}/maintain/finish/" + ss.id);
                //清楚session

            });


        </script>
        <script>

            /**
             * layui表单
             */
            layui.use('form', function () {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function (data) {
                    // alert(JSON.stringify(data.field));
                    // alert($("#hidInp").val());
                    var ss = sessionStorage.getItem('maintain');

                    //解决未定义问题
                    ss = eval("(" + ss + ")");

                    // console.log(ss.id);


                    //异步请求
                    $.ajax({
                        url: $("#hidInp").val(),
                        type: "post",
                        data: JSON.stringify(data.field),
                        contentType: 'application/json;charset=utf-8',
                        dataType: "text",
                        success: function (msg) {
                            msg = "true";
                            if (msg === "true") {
                                // console.log(43);
                                // console.log(ss.carNumber + $("#input2").val());
                                $.ajax({
                                    url: 'http://192.168.43.87:8080/Car/SerUpdate',
                                    type: "post",
                                    data: {'carnum': ss.carNumber, 'note': $("#input2").val()},
                                    // contentType: 'application/json;charset=utf-8',
                                    dataType: "text",
                                    success: function (msg) {
                                        console.log(msg);
                                        console.log(77);
                                        sessionStorage.removeItem('maintain');
                                    }

                                });
                            } else {
                                sessionStorage.removeItem('maintain');
                            }
                            //关闭当前窗口
                            closeMy();

                            //刷新父页面
                            window.parent.location.reload();


                        }
                    });
                    return false;
                });


            });
        </script>
    </body>
</html>
