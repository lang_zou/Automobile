<%--
  Created by IntelliJ IDEA.
  User: 皮皮
  Date: 2021/4/21
  Time: 9:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>汽车维修管理系统</title>
        <meta name="keywords" content="qadmin,qadmin模板,后台模板,qadmin后台模板">
        <meta name="description" content="qadmin是一个轻量级后台模板,基于layui框架开发,简洁而不简单">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/layui/css/layui.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/qadmin-iframe/static/admin/css/style.css">
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/layui/layui.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/qadmin-iframe/static/js/vue.min.js"></script>
    </head>
    <body>

        <fieldset class="layui-elem-field layui-field-title">
            <legend id="hh">添加汽车信息</legend>
        </fieldset>

        <form class="layui-form" action="${pageContext.request.contextPath}/views/car-list.jsp">

            <%--          存放表单提交方式  --%>
            <input hidden id="hidInp" value="${pageContext.request.contextPath}/car/">

            <div class="layui-form-item">
                <label class="layui-form-label">汽车品牌</label>
                <div class="layui-input-block">
                    <input id="input1" type="text" name="carBrand" required lay-verify="required" placeholder="请输入"
                           autocomplete="off" class="layui-input" value="${car.carBrand}">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">车牌号</label>
                <div class="layui-input-block">
                    <input type="text" name="carNumber" required lay-verify="required|carNumber" placeholder="不要输入小写字母"
                           autocomplete="off" class="layui-input" value="${car.carNumber}">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">客户姓名</label>
                <div class="layui-input-block">
                    <input type="text" name="clientName" required lay-verify="required" placeholder="请输入"
                           autocomplete="off" class="layui-input" value="${car.clientName}">
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    <button class="layui-btn layui-btn-primary"
                            onclick="window.location.href='${pageContext.request.contextPath}/views/car-list.jsp'">返回
                    </button>

                </div>
            </div>
        </form>

        <script>
            /**
             * 初始化函数
             */
            $(function () {
                <%--    // alert($("#input1").val());--%>
                change();

            });


            function change() {
                // alert($("#input1").val());
                if ($("#input1").val() !== "") {
                    // alert(666);
                    $("#hh").text("修改汽车信息");
                    $("#hidInp").val("${pageContext.request.contextPath}/car/${car.carId}");
                }
            }
        </script>
        <script>

            /**
             * layui表单
             */
            layui.use('form', function () {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function (data) {
                    // alert(JSON.stringify(data.field));
                    //异步请求
                    $.ajax({
                        url: $("#hidInp").val(),
                        type: "post",
                        data: JSON.stringify(data.field),
                        contentType: 'application/json;charset=utf-8',
                        dataType: "text",
                        success: function (msg) {
                            parent.location.reload();
                        }
                    });
                });

                //自定义表单验证
                form.verify({
                    carNumber: function (value, item) { //value：表单的值、item：表单的DOM对象
                        var regExp = /(^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$)/;
                        if (!regExp.test(value)) {
                            return '请输入正确的车牌号';
                        }

                    }

                });
            });
        </script>
    </body>
</html>
