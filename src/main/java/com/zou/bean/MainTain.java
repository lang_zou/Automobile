package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 维修记录表
 *
 * @author
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MainTain implements Serializable {

  /** 维修Id */
  private Integer id;

  /** 维修编号 */
  private String maintainNo;

  /** 汽车Id */
  private Integer carId;

  /** 车牌号 */
  private String carNumber;

  /** 客户姓名 */
  private String clientName;

  /** 维修日期 */
  private Date maintainDate;

  /** 完工日期 */
  private Date finishDate;

  /** 维修详情 */
  private String maintainInfo;

  /** 维修费用 */
  private Integer maintainCost;

  /** 维修状态 */
  private Integer state;

  /** 维修人员Id */
  private Integer accendantId;

  /** 维修人员姓名 */
  private String accendantName;

  /** 配件名称集合 */
  private List<PartsRepertory> parts;

  /** 是否启用 */
  private int isEnable;

  public MainTain(Integer carId, String maintainInfo, Integer accendantId) {
    this.carId = carId;
    this.maintainInfo = maintainInfo;
    this.accendantId = accendantId;
  }

  public MainTain(Integer id, Integer state) {
    this.id = id;
    this.state = state;
  }
}
