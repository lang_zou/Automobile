package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * userinfo
 *
 * @author
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo implements Serializable {

  /** 用户Id */
  private Integer userId;

  /** 用户名 */
  private String userName;

  /** 登录名 */
  private String loginName;

  /** 用户密码 */
  private String userPwd;

  /** 创建日期 */
  private Date createDate;

  /** 登录状态 */
  private Integer loginState;

  /** 是否删除 */
  private Integer isDelete;

  public UserInfo(Integer userId, String userName, String loginName, String userPwd) {
    this.userId = userId;
    this.userName = userName;
    this.loginName = loginName;
    this.userPwd = userPwd;
  }

  public UserInfo(String userName, String loginName, String userPwd) {
    this.userName = userName;
    this.loginName = loginName;
    this.userPwd = userPwd;
  }

  public UserInfo(String loginName, String userPwd) {
    this.loginName = loginName;
    this.userPwd = userPwd;
  }

  public UserInfo(Integer userId, String userName, String loginName) {
    this.userId = userId;
    this.userName = userName;
    this.loginName = loginName;
  }
}
