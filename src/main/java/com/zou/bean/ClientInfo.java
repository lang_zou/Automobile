package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * clientinfo
 *
 * @author
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientInfo implements Serializable {

  /** 客户Id */
  private Integer clientId;

  /** 客户姓名 */
  private String clientName;

  /** 性别 */
  private String sex;

  /** 手机号 */
  private String phone;

  /** 创建日期 */
  private Date createDate;

  public ClientInfo(Integer clientId, String clientName, String sex, String phone) {
    this.clientId = clientId;
    this.clientName = clientName;
    this.sex = sex;
    this.phone = phone;
  }
}
