package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 采购表 purchase
 *
 * @author
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Purchase implements Serializable {

  /** 采购Id */
  private Integer purchaseId;

  /** 采购编号 */
  private Integer purchaseNo;

  /** 配件信息Id */
  private Integer partsInfoId;

  /** 配件类别 */
  private String partsType;

  /** 配件名称 */
  private String partsName;

  /** 采购数量 */
  private Integer purchaseCount;

  /** 采购日期d */
  private Date purchaseDate;

  /** 采购总金额 */
  private Integer purchaseSumMoney;

  public Purchase(
      Integer purchaseNo,
      Integer partsInfoId,
      String partsType,
      String partsName,
      Integer purchaseCount,
      Integer purchaseSumMoney) {
    this.purchaseNo = purchaseNo;
    this.partsInfoId = partsInfoId;
    this.partsType = partsType;
    this.partsName = partsName;
    this.purchaseCount = purchaseCount;
    this.purchaseSumMoney = purchaseSumMoney;
  }
}
