package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** 新增工单时，获取配件集合 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class partsList {
  //    "value":2,"title":"油底壳","disabled":false
  /** VAlue */
  private int value;

  /** 配件名称 */
  private String title;

  /** 是否启用 */
  //  private String disabled;
}
