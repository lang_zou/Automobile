package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * roleinfo
 *
 * @author
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleInfo implements Serializable {

  /** 角色Id */
  private Integer roleId;

  /** 角色名 */
  private String roleName;

  /** 角色描述 */
  private String roleDesc;

  /** 是否启用 */
  private Integer isEnable;

  public RoleInfo(Integer roleId, String roleName, String roleDesc) {
    this.roleId = roleId;
    this.roleName = roleName;
    this.roleDesc = roleDesc;
  }

  public RoleInfo(String roleName, String roleDesc) {
    this.roleName = roleName;
    this.roleDesc = roleDesc;
  }
}
