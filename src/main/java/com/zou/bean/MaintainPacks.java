package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/** 解决新增工单的问题 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MaintainPacks {

  /** 维修记录 */
  private MainTain maintain;

  /** 使用配件 */
  private List<partsList> parts;
}
