package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * subscribe
 *
 * @author
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subscribe implements Serializable {

  /** 预约ID */
  private Integer id;

  /** 客户ID */
  private Integer cilentId;

  /** 预约日期 */
  private Date subscribeDate;

  /** 预约信息 */
  private String subscribeInfo;
}
