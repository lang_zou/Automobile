package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * partsrepertory
 *
 * @author
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartsRepertory implements Serializable {

  /** 配件Id */
  private Integer partsId;

  /** 配件名 */
  private String partsName;

  /** 配件类型 */
  private String partsType;

  /** 配件售卖价 */
  private Integer partsPrice;

  /** 配件数量 */
  private Integer count;

  public PartsRepertory(Integer partsId, Integer partsPrice) {
    this.partsId = partsId;
    this.partsPrice = partsPrice;
  }

  public PartsRepertory(String partsName, Integer count) {
    this.partsName = partsName;
    this.count = count;
  }
}
