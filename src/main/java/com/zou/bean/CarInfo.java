package com.zou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * carinfo
 *
 * @author
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarInfo implements Serializable {

  /** 汽车Id */
  private Integer carId;

  /** 客户Id */
  private Integer clientId;

  /** 汽车品牌 */
  private String carBrand;

  /** 车牌号 */
  private String carNumber;

  /** 姓名 */
  private String clientName;

  /** 客户信息 */
  private ClientInfo clientInfo;

  public CarInfo(String carBrand, String carNumber) {
    this.carBrand = carBrand;
    this.carNumber = carNumber;
  }

  public CarInfo(Integer carId, Integer clientId, String carBrand, String carNumber) {
    this.carId = carId;
    this.clientId = clientId;
    this.carBrand = carBrand;
    this.carNumber = carNumber;
  }

  public CarInfo(Integer carId, String carBrand, String carNumber, String clientName) {
    this.carId = carId;
    this.carBrand = carBrand;
    this.carNumber = carNumber;
    this.clientName = clientName;
  }
}
