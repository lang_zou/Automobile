package com.zou.controller;

import com.zou.bean.Resources;
import com.zou.bean.UserInfo;
import com.zou.service.ResourceService;
import com.zou.service.RoleInfoService;
import com.zou.service.UserInfoService;
import com.zou.utils.ToMD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/** 控制层 */
@Controller
@CrossOrigin
public class MainController {

  //  http://localhost/Automobile

  // 用户表业务层对象
  @Autowired private UserInfoService uService;

  // 资源表服务层实例
  @Autowired ResourceService resService;

  // 角色表服务层实例
  @Autowired RoleInfoService roleService;

  /** 测试 */
  @RequestMapping("/test")
  @ResponseBody
  public String test() {

    return " ";
  }

  /**
   * 登录跳转主页面
   *
   * @return
   */
  @RequestMapping("/index")
  public String login() {
    return "forward:/WEB-INF/views/index.jsp";
  }

  /** 退出登录清除session */
  @RequestMapping("/out")
  public String out(HttpSession session) {
    // 清除所有session
    session.invalidate();
    return "redirect:/";
  }

  /** 跳转登录页面 */
  @RequestMapping("/login")
  public String toLogin() {
    return "forward:/WEB-INF/views/login.jsp";
  }

  /**
   * 登录验证
   *
   * @param loginName
   * @param userPwd
   * @return
   */
  @RequestMapping(value = "/index", method = RequestMethod.POST)
  public ModelAndView loginCheck(String loginName, String userPwd, HttpSession session) {

    // 输出用户名密码
    //    System.out.println(loginName + ":" + userPwd);

    // 调用验证方法，返回一个用户对象
    UserInfo userInfo = uService.verify(new UserInfo(loginName, ToMD5.to(userPwd)));

    // 如果验证成功
    if (userInfo != null) {

      // 放入用户信息
      session.setAttribute("loginUser", userInfo);

      // 放入菜单信息
      //      session.setAttribute("menu",
      // genMenu(resService.selectResByUser(userInfo.getUserId())));

      // 放入角色信息
      session.setAttribute("roles", roleService.selectRolesByu(userInfo.getUserId()));

      // 跳转页面
      return new ModelAndView("forward:/WEB-INF/views/index.jsp");
    } else {

      // 用户名或密码错误，返回登录界面
      return new ModelAndView("redirect:/");
    }
  }

  /** iframe展示首页 */
  @RequestMapping("/main")
  public String showMain() {
    return "forward:/WEB-INF/views/main.jsp";
  }

  /**
   * 返回菜单
   *
   * @param res
   * @return
   */
  public static String genMenu(List<Resources> res) {
    StringBuilder str =
        new StringBuilder(
            "[\n"
                + "                {\n"
                + "                \"name\": \"\",\n"
                + "                \"icon\": \"&#xe68e;\",\n"
                + "                \"url\": \"\",\n"
                + "                \"hidden\": true,\n"
                + "                \"list\": [{\n"
                + "                    \"name\": \"首页\",\n"
                + "                    \"url\": \"${pageContext.request.contextPath}/main\",\n"
                + "                }]\n"
                + "            }");

    str.append(
        " , {\n"
            + "                    \"name\": \"退出登录\",\n"
            + "                    \"icon\": \"&#xe65c;\",\n"
            + "                    \"url\": \"${pageContext.request.contextPath}/out\",\n"
            + "                    \"list\": []\n"
            + "                }\n"
            + "            ]");
    return str.toString();
  }
}
