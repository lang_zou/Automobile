package com.zou.controller;

import com.zou.bean.*;
import com.zou.service.*;
import com.zou.utils.TableConvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

@Controller
@CrossOrigin
@RequestMapping("/maintain")
public class MaintainController {

  // 创建服务层对象
  @Autowired private MaintainService mService;

  // 创建服务层对象
  @Autowired private PartsUseService uService;

  // 创建库存服务层对象
  @Autowired private RepertoryService rService;

  // 创建汽车业务层对象
  @Autowired private CarInfoService carService;

  // 创建客户服务层对象
  @Autowired private ClientService cliService;

  /** 商业合作（与租车公司） 开始修车 */
  @RequestMapping(value = "/work", method = RequestMethod.POST)
  @ResponseBody
  public String specialWork(String carBrand, String carNumber) {

    // 非空判断
    if (carBrand == null || carNumber == null) {

    } else {

      // 往汽车表里添加数据（如果已有该汽车，则不用新增）
      CarInfo car = new CarInfo(carBrand, carNumber);

      // 设置客户Id
      car.setClientId(28);
      //    car.setClientId(cliService.selectId("宇健租车公司"));

      try {
        carService.insertSelective(car);
      } catch (Exception e) {
        System.out.println("已有该车……");
      }

      // 往维修记录表里添加数据
      // 设置维修状态（配件待出库）
      MainTain maintain = new MainTain(carService.selectIdByNum(carNumber), "这是宇健租车公司的车，请认真对待！", 7);

      // 如果用到配件
      maintain.setState(2);

      // 插入数据并查出维修Id
      int maintainId = mService.insertSelective(maintain);

      //    for (partsList part : new PartsRepertory) {
      uService.insertSelective(new PartsUse(maintainId, 3));
      uService.insertSelective(new PartsUse(maintainId, 7));
      uService.insertSelective(new PartsUse(maintainId, 18));
      //    }
    }
    return "success";
  }

  /**
   * 查询所有已完成工单
   *
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/out", method = RequestMethod.GET)
  public Map selectAllOut() {
    return TableConvent.toDo(mService.selectAllOut());
  }

  /** 新增工单 */
  @ResponseBody
  @RequestMapping(value = "/", method = RequestMethod.POST)
  public String add(@RequestBody MaintainPacks pack) {

    // 如果有配件
    if (pack.getParts().size() > 0) {

      // 设置维修状态（配件待出库）
      pack.getMaintain().setState(2);

      // 插入数据并查出维修Id
      int maintainId = mService.insertSelective(pack.getMaintain());

      //      System.out.println(pack.getParts());

      for (partsList part : pack.getParts()) {

        // 遍历配件集合，往配件使用表中插入数据
        uService.insertSelective(new PartsUse(maintainId, part.getValue()));
      }

      // 么有配件时
    } else {

      // 设置维修状态（维修中）
      pack.getMaintain().setState(0);

      // 插入数据并查出维修Id
      //      int maintainId = mService.insertSelective(pack.getMaintain());
    }
    return "s";
  }

  /**
   * 查询所有已完成工单
   *
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/his", method = RequestMethod.GET)
  public Map selectAllHis() {
    return TableConvent.toDo(mService.selectAllHis());
  }

  /** 完工前跳转页面 */
  @RequestMapping(value = "/finish", method = RequestMethod.POST)
  @ResponseBody
  public MainTain test(@RequestBody MainTain maintain) {
    return maintain;
  }

  /** 完工 */
  @ResponseBody
  @RequestMapping(value = "/finish/{id}", method = RequestMethod.POST)
  public String finish(@PathVariable int id, @RequestBody MainTain maintain) {

    //    System.out.println(mService.selectClientId(id));

    // 设置维修记录Id
    maintain.setId(id);

    // 设置维修状态
    maintain.setState(1);

    // 设置完工日期
    maintain.setFinishDate(new Date(System.currentTimeMillis()));

    // 更新维修记录
    mService.updateByPrimaryKeySelective(maintain);

    // 完工后减少库存配件数量
    rService.updateRepCount(id);

    // 判断是否是租车公司的车
    if (mService.selectClientId(id) == 28) {
      return "true";
    }

    return " e";
  }

  /** 取消 */
  @ResponseBody
  @RequestMapping(value = "/cancel/{id}", method = RequestMethod.DELETE)
  public void cancel(@PathVariable int id) {
    mService.cancel(id);
  }

  /** 删除 */
  @ResponseBody
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void delete(@PathVariable int id) {
    mService.deleteByPrimaryKey(id);
  }

  /**
   * 查询所有
   *
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public Map selectAll() {
    return TableConvent.toDo(mService.selectAll());
  }
}
