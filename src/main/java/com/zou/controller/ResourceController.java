package com.zou.controller;

import com.zou.bean.Resources;
import com.zou.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping(value = "/resource") // 通过这里配置使下面的映射都在/Resource下
public class ResourceController {

  // 资源表服务层实例
  @Autowired ResourceService resService;

  /**
   * 查询
   *
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public List<Resources> getResourceList() {
    return resService.selectAll();
  }

  /**
   * 新增
   *
   * @param resName
   * @return
   */
  @RequestMapping(value = "/", method = RequestMethod.POST)
  public String postResource(String resName, String resUrl) {

    // 插入数据
    resService.insertSelective(new Resources(resName, resUrl));

    // 跳转页面
    return "res-list";
  }

  /**
   * 更新前跳转页面
   *
   * @param id
   * @return
   */
  @RequestMapping(value = "/jump/{id}", method = RequestMethod.POST)
  public ModelAndView postResource2(@PathVariable int id, String resName, String resUrl) {
    return new ModelAndView("res-add").addObject("res", new Resources(id, resName, resUrl));
  }

  /**
   * 更新
   *
   * @param id
   * @param
   * @return
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.POST)
  public String putResource(@PathVariable int id, String resName, String resUrl) {

    // 更新数据
    resService.updateByPrimaryKeySelective(new Resources(id, resName, resUrl));

    // 跳转页面
    return "res-list";
  }

  /**
   * 删除
   *
   * @param id
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public String deleteResource(@PathVariable int id) {

    // 执行删除
    resService.deleteByPrimaryKey(id);

    // 返回数据
    return "success";
  }
}
