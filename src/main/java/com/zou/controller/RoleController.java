package com.zou.controller;

import com.zou.bean.RoleInfo;
import com.zou.service.ResourceService;
import com.zou.service.RoleInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping(value = "/role")
public class RoleController {

  // 资源表服务层实例
  @Autowired RoleInfoService roleService;

  // 角色资源表服务层实例
  //  @Autowired RoleResourceService roleResService;

  // 资源表服务层实例
  @Autowired ResourceService resService;

  /**
   * 更新
   *
   * @param id
   * @param
   * @return
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.POST)
  public String putResource(@PathVariable int id, String roleName, String roleDesc) {

    // 更新角色表
    roleService.updateByPrimaryKeySelective(new RoleInfo(id, roleName, roleDesc));

    // 跳转页面
    return "role-list";
  }

  /**
   * 更新前跳转页面
   *
   * @param id
   * @return
   */
  @RequestMapping(value = "/jump/{id}", method = RequestMethod.POST)
  public ModelAndView postRole2(@PathVariable int id, String roleName, String roleDesc) {
    return new ModelAndView("role-add").addObject("role", new RoleInfo(id, roleName, roleDesc));
  }

  /**
   * 新增
   *
   * @param
   * @return
   */
  @RequestMapping(value = "/", method = RequestMethod.POST)
  public String postResource(String roleName, String roleDesc) {
    // 处理POST请求，用来新增

    // 插入新的角色
    roleService.insertSelective(new RoleInfo(roleName, roleDesc));

    // 跳转页面
    return "role-list";
  }

  /**
   * 删除
   *
   * @param id
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public String deleteResource(@PathVariable int id) {
    // 处理DELETE请求，用来删除Resource

    // 执行删除方法
    roleService.deleteByPrimaryKey(id);

    // 返回数据
    return "success";
  }

  /**
   * 查询所有角色信息(不包含资源)
   *
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public List<RoleInfo> getById() {
    return roleService.selectAll();
  }
}
