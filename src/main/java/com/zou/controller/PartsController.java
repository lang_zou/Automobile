package com.zou.controller;

import com.zou.bean.MainTain;
import com.zou.bean.PartsRepertory;
import com.zou.bean.Purchase;
import com.zou.service.MaintainService;
import com.zou.service.PurchaseService;
import com.zou.service.RepertoryService;
import com.zou.utils.TableConvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/parts")
public class PartsController {

  // 创建服务层对象
  @Autowired private MaintainService mService;

  // 配件库存业务层对象
  @Autowired RepertoryService repService;

  // 采购业务层对象
  @Autowired PurchaseService pService;

  /** 更新前跳转页面 */
  @RequestMapping(value = "/rep/", method = RequestMethod.POST)
  @ResponseBody
  public PartsRepertory test(@RequestBody PartsRepertory parts) {
    return parts;
  }

  /** 修改配件价格 */
  @RequestMapping(value = "/rep/{id}", method = RequestMethod.POST)
  public String finish(@PathVariable int id, int partsPrice) {

    // 更新库存表
    repService.updateByPrimaryKeySelective(new PartsRepertory(id, partsPrice));

    // 跳转页面
    return "parts-manage";
  }

  /** 配件出库 */
  @ResponseBody
  @RequestMapping(value = "/out/{id}", method = RequestMethod.GET)
  public String partsOut(@PathVariable int id) {

    // 更新维修记录表
    mService.updateByPrimaryKeySelective(new MainTain(id, 0));

    // 返回数据
    return "success";
  }

  /** 新增采购 */
  @ResponseBody
  @RequestMapping(value = "/pur/add", method = RequestMethod.POST)
  public String addPur(String partsInfo, String purchaseCount) {

    // 解析配件信息
    String[] parts = partsInfo.split("\\|");

    // 插入采购表
    pService.insertSelective(
        new Purchase(
            pService.selectId() + 1,
            Integer.parseInt(parts[0]),
            parts[1],
            parts[2],
            Integer.parseInt(purchaseCount),
            Integer.parseInt(purchaseCount) * Integer.parseInt(parts[3])));

    // 返回数据
    return "success";
  }

  /** 查询所有配件信息 */
  @ResponseBody
  @RequestMapping(value = "/pur/parts", method = RequestMethod.GET)
  public List<Purchase> selectAllparts() {
    //    int n = 1 / 0;
    return pService.selectAllParts();
  }

  /** 查询指定类型的所有配件 */
  @ResponseBody
  @RequestMapping(value = "/pur/{partsType}", method = RequestMethod.GET)
  public List<Purchase> selectByTypeAll(@PathVariable String partsType) {
    return pService.selectByType(partsType);
  }

  /** 查询采购记录 */
  @ResponseBody
  @RequestMapping(value = "/pur", method = RequestMethod.GET)
  public Map selectPur() {
    return TableConvent.toDo(pService.selectAll());
  }

  /** 查询指定类型的库存配件 */
  @ResponseBody
  @RequestMapping(value = "/rep/{partsType}", method = RequestMethod.GET)
  public List<PartsRepertory> selectByType(@PathVariable String partsType) {
    return repService.selectByType(partsType);
  }

  /** 删除库存配件 */
  @ResponseBody
  @RequestMapping(value = "rep/{id}", method = RequestMethod.DELETE)
  public void delete(@PathVariable int id) {
    repService.deleteByPrimaryKey(id);
  }

  /**
   * 新增配件
   *
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/rep", method = RequestMethod.POST)
  public void add(@RequestBody PartsRepertory rep) {
    //    System.out.println(rep);

    // 插入数据
    repService.insertSelective(rep);
  }

  /**
   * 查询所有配件库存信息
   *
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/rep", method = RequestMethod.GET)
  public Map selectAll() {
    return TableConvent.toDo(repService.selectAll());
  }
}
