package com.zou.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** 登录拦截器 */
public class LoginInterceptor implements HandlerInterceptor {

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {

    //    System.out.println("进入拦截器");
    //    System.out.println(request.getMethod());
    // 不拦截post请求
    if (request.getMethod().equals("POST")) {
      return true;
    }

    // 如果没有session,就拦截
    if (request.getSession().getAttribute("loginUser") == null) {

      //      System.out.println("拦截到");

      // 返回一个不可逆的页面
      response
          .getWriter()
          .append(
              "<script>if(self != top){parent.window.location.href='/Automobile/';}else{window.location.href='/Automobile/';}</script>");

      return false;
    }

    return true;
  }
}
