package com.zou.interceptor;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

// 注解式通知
@ControllerAdvice
public class ExceptionControllerAdvice {

  // 异常统一处理
  @ExceptionHandler(Exception.class)
  public String myExceptionHandler(Exception e) {
    //        Log.error(e.getStackTrace().toString());

    // 清除所有session
    //    session.invalidate();

    // 跳转到错误页面
    return "forward:/views-common/error.jsp";
  }
}
