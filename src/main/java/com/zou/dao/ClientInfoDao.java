package com.zou.dao;

import com.zou.bean.ClientInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientInfoDao {

  /** 查询所有 */
  List<ClientInfo> selectAll();

  /** 查询指定客户名的客户Id */
  int selectId(String clientName);

  /**
   * 通过主键删除
   *
   * @param clientId
   * @return
   */
  int deleteByPrimaryKey(Integer clientId);

  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(ClientInfo record);

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  int insertSelective(ClientInfo record);

  /**
   * 查询
   *
   * @param clientId
   * @return
   */
  ClientInfo selectByPrimaryKey(Integer clientId);

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(ClientInfo record);

  /**
   * 更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKey(ClientInfo record);
}
