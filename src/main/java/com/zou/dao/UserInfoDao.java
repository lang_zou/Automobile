package com.zou.dao;

import com.zou.bean.UserInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserInfoDao {

  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(UserInfo record);

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  int insertSelective(UserInfo record);

  /**
   * 删除(逻辑删除不用此方法)
   *
   * @param userId
   * @return
   */
  int deleteByPrimaryKey(Integer userId);

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(UserInfo record);

  /**
   * 查询所有
   *
   * @return
   */
  List<UserInfo> selectAll();

  /** 登录验证 */
  UserInfo verify(UserInfo u);
}
