package com.zou.dao;

import com.zou.bean.PartsRepertory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepertoryDao {

  /** 更新库存配件数量 */
  int updateCount(PartsRepertory record);

  /** 查询配件名称对应的库存Id */
  int selectId(String partsName);

  /** 查询指定类型的配件信息 */
  List<PartsRepertory> selectByType(String partsType);

  /** 查询所有 */
  List<PartsRepertory> selectAll();

  /**
   * 删除
   *
   * @param partsId
   * @return
   */
  int deleteByPrimaryKey(Integer partsId);

  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(PartsRepertory record);

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  int insertSelective(PartsRepertory record);

  /**
   * 查询
   *
   * @param partsId
   * @return
   */
  PartsRepertory selectByPrimaryKey(Integer partsId);

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(PartsRepertory record);

  /**
   * 更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKey(PartsRepertory record);

  /** 更新库存量根据配件Id */
  void updateByPartId(int id);
}
