package com.zou.utils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/** 字符串处理 */
public class TableConvent {

  /**
   * 针对layUI的table单独定制返回类型返回数据格式工具类
   *
   * @param list 传入集合数据 // * @param count 传入数据总条数
   * @return
   */
  public static Map<String, Object> toDo(List<?> list) {

    // 实例化一个Map
    Map<String, Object> map = new LinkedHashMap<>();

    // 往map里放入数据
    map.put("code", 0);
    map.put("msg", "");
    map.put("count", list.size());
    map.put("data", list);

    return map;
  }
}
