package com.zou.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/** md5加密 */
public class ToMD5 {

  /**
   * 加密(MD5加密)
   *
   * @return String
   */
  public static String to(String data) {

    // 实例化一个大数据
    BigInteger bigInteger = null;
    try {

      // 实例化MD5对象
      MessageDigest md = MessageDigest.getInstance("MD5");

      // 创建一个字节数组
      byte[] dataBytes = data.getBytes();

      // 往md中放入字节
      md.update(dataBytes);

      // 处理大数据
      bigInteger = new BigInteger(md.digest());

      // 返回16位的字符串
      return bigInteger.toString(16);

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return null;
  }
}
