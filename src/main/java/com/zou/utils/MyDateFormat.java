package com.zou.utils;

import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Date;

/** 前端传入后端（日期处理） */
public class MyDateFormat implements Converter<String, Date> {

  /**
   * 日期转换
   *
   * @param s
   * @return
   */
  @Override
  public Date convert(String s) {

    // 定义一个日期格式
    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

    try {

      // 返回格式化后的字符串
      return sf.parse(s);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
