package com.zou.service;

import com.zou.bean.RoleInfo;

import java.util.List;

public interface RoleInfoService {
  /** 查询指定用户的角色 */
  List<RoleInfo> selectRolesByu(int userId);
  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(RoleInfo record);

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  int insertSelective(RoleInfo record);

  /**
   * 删除
   *
   * @param roleId
   * @return
   */
  int deleteByPrimaryKey(Integer roleId);

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(RoleInfo record);

  /**
   * 查询所有
   *
   * @return
   */
  List<RoleInfo> selectAll();
}
