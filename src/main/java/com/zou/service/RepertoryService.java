package com.zou.service;

import com.zou.bean.PartsRepertory;

import java.util.List;

public interface RepertoryService {

  /** 查询所有 */
  List<PartsRepertory> selectAll();

  /** 查询指定类型的配件信息 */
  List<PartsRepertory> selectByType(String partsType);
  /**
   * 删除
   *
   * @param partsId
   * @return
   */
  int deleteByPrimaryKey(Integer partsId);

  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(PartsRepertory record);

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  int insertSelective(PartsRepertory record);

  /**
   * 查询
   *
   * @param partsId
   * @return
   */
  PartsRepertory selectByPrimaryKey(Integer partsId);

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(PartsRepertory record);

  /**
   * 更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKey(PartsRepertory record);

  /**
   * 根据维修Id更新库存数量
   *
   * @param
   * @return
   */
  void updateRepCount(int id);
}
