package com.zou.service;

import com.zou.bean.Accendant;

import java.util.List;

public interface AccendantService {

  /**
   * 删除
   *
   * @param accendantId
   * @return
   */
  int deleteByPrimaryKey(Integer accendantId);

  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(Accendant record);

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  int insertSelective(Accendant record);

  /**
   * 查询
   *
   * @param accendantId
   * @return
   */
  Accendant selectByPrimaryKey(Integer accendantId);

  /**
   * 查询所有
   *
   * @return
   */
  List<Accendant> selectAll();

  /**
   * 更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(Accendant record);

  /**
   * 更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKey(Accendant record);
}
