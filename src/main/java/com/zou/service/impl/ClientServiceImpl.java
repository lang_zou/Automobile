package com.zou.service.impl;

import com.zou.bean.ClientInfo;
import com.zou.dao.ClientInfoDao;
import com.zou.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

  // 创建dao层对象
  @Autowired private ClientInfoDao dao;

  /** 查询所有 */
  @Override
  public List<ClientInfo> selectAll() {
    return dao.selectAll();
  }

  /**
   * 查询指定客户名的客户Id
   *
   * @param clientName
   */
  @Override
  public int selectId(String clientName) {
    return dao.selectId(clientName);
  }

  /**
   * 通过主键删除
   *
   * @param clientId
   * @return
   */
  @Override
  public int deleteByPrimaryKey(Integer clientId) {
    return dao.deleteByPrimaryKey(clientId);
  }

  /**
   * 插入
   *
   * @param record
   * @return
   */
  @Override
  public int insert(ClientInfo record) {
    return dao.insert(record);
  }

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  @Override
  public int insertSelective(ClientInfo record) {
    return dao.insertSelective(record);
  }

  /**
   * 查询
   *
   * @param clientId
   * @return
   */
  @Override
  public ClientInfo selectByPrimaryKey(Integer clientId) {
    return dao.selectByPrimaryKey(clientId);
  }

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKeySelective(ClientInfo record) {
    return dao.updateByPrimaryKeySelective(record);
  }

  /**
   * 更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKey(ClientInfo record) {
    return dao.updateByPrimaryKey(record);
  }
}
