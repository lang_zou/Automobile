package com.zou.service.impl;

import com.zou.bean.PartsRepertory;
import com.zou.dao.PartsUseDao;
import com.zou.dao.RepertoryDao;
import com.zou.service.RepertoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RepertoryServiceImpl implements RepertoryService {

  // 实例化Dao层对象
  @Autowired RepertoryDao dao;

  // 实例化Dao层对象
  @Autowired PartsUseDao pDao;

  /** 查询所有 */
  @Override
  public List<PartsRepertory> selectAll() {
    return dao.selectAll();
  }

  /**
   * 查询指定类型的配件信息
   *
   * @param partsType
   */
  @Override
  public List<PartsRepertory> selectByType(String partsType) {
    return dao.selectByType(partsType);
  }

  /**
   * 删除
   *
   * @param partsId
   * @return
   */
  @Override
  public int deleteByPrimaryKey(Integer partsId) {
    return dao.deleteByPrimaryKey(partsId);
  }

  /**
   * 插入
   *
   * @param record
   * @return
   */
  @Override
  public int insert(PartsRepertory record) {
    return dao.insert(record);
  }

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  @Override
  public int insertSelective(PartsRepertory record) {
    return dao.insertSelective(record);
  }

  /**
   * 查询
   *
   * @param partsId
   * @return
   */
  @Override
  public PartsRepertory selectByPrimaryKey(Integer partsId) {
    return dao.selectByPrimaryKey(partsId);
  }

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKeySelective(PartsRepertory record) {
    return dao.updateByPrimaryKeySelective(record);
  }

  /**
   * 更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKey(PartsRepertory record) {
    return dao.updateByPrimaryKey(record);
  }

  /**
   * 根据维修Id更新库存数量
   *
   * @param id@return
   */
  @Override
  public void updateRepCount(int id) {

    // 获取配件Id
    for (Integer partId : pDao.selectById(id)) {

      // 更改配件数量
      dao.updateByPartId(partId);
    }
  }
}
