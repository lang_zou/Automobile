package com.zou.service.impl;

import com.zou.bean.UserInfo;
import com.zou.dao.UserInfoDao;
import com.zou.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {

  // 创建Dao层对象
  @Autowired private UserInfoDao dao;

  /**
   * 插入
   *
   * @param record
   * @return
   */
  @Override
  public int insert(UserInfo record) {
    return dao.insert(record);
  }

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  @Override
  public int insertSelective(UserInfo record) {
    return dao.insertSelective(record);
  }

  /**
   * 删除(逻辑删除不用此方法)
   *
   * @param userId
   * @return
   */
  @Override
  public int deleteByPrimaryKey(Integer userId) {
    return dao.deleteByPrimaryKey(userId);
  }

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKeySelective(UserInfo record) {
    return dao.updateByPrimaryKeySelective(record);
  }

  /**
   * 查询所有
   *
   * @return
   */
  @Override
  public List<UserInfo> selectAll() {
    return dao.selectAll();
  }

  /**
   * 登录验证
   *
   * @param u
   */
  @Override
  public UserInfo verify(UserInfo u) {
    return dao.verify(u);
  }
}
