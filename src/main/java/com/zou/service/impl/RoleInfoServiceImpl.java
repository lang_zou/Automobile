package com.zou.service.impl;

import com.zou.bean.RoleInfo;
import com.zou.dao.RoleInfoDao;
import com.zou.service.RoleInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoleInfoServiceImpl implements RoleInfoService {

  // 创建dao层对象
  @Autowired private RoleInfoDao dao;

  /**
   * 查询指定用户的角色
   *
   * @param userId
   */
  @Override
  public List<RoleInfo> selectRolesByu(int userId) {
    return dao.selectRolesByu(userId);
  }

  /**
   * 插入
   *
   * @param record
   * @return
   */
  @Override
  public int insert(RoleInfo record) {
    return dao.insert(record);
  }

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  @Override
  public int insertSelective(RoleInfo record) {
    return dao.insertSelective(record);
  }

  /**
   * 删除
   *
   * @param roleId
   * @return
   */
  @Override
  public int deleteByPrimaryKey(Integer roleId) {
    return dao.deleteByPrimaryKey(roleId);
  }

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKeySelective(RoleInfo record) {
    return dao.updateByPrimaryKeySelective(record);
  }

  /**
   * 查询所有
   *
   * @return
   */
  @Override
  public List<RoleInfo> selectAll() {
    return dao.selectAll();
  }
}
