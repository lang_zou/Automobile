package com.zou.service.impl;

import com.zou.bean.PartsRepertory;
import com.zou.bean.Purchase;
import com.zou.dao.PurchaseDao;
import com.zou.dao.RepertoryDao;
import com.zou.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseServiceImpl implements PurchaseService {

  // 实例化Dao层对象
  @Autowired private PurchaseDao dao;

  // 实例化Dao层对象
  @Autowired private RepertoryDao repDao;

  /** 查询最大的采购编号 */
  @Override
  public int selectId() {
    return dao.selectId();
  }

  /** 查询所有的配件信息 */
  @Override
  public List<Purchase> selectAllParts() {
    return dao.selectAllParts();
  }

  /**
   * 查询指定类型的配件信息
   *
   * @param partsType
   */
  @Override
  public List<Purchase> selectByType(String partsType) {
    return dao.selectByType(partsType);
  }

  /** 查询所有 */
  @Override
  public List<Purchase> selectAll() {
    return dao.selectAll();
  }

  /**
   * 删除
   *
   * @param purchaseId
   * @return
   */
  @Override
  public int deleteByPrimaryKey(Integer purchaseId) {
    return dao.deleteByPrimaryKey(purchaseId);
  }

  /**
   * 插入
   *
   * @param record
   * @return
   */
  @Override
  public int insert(Purchase record) {
    return dao.insert(record);
  }

  /**
   * 可选择插入
   *
   * @param record
   * @return
   */
  @Override
  public int insertSelective(Purchase record) {

    // 增加配件库存数量
    repDao.updateCount(new PartsRepertory(record.getPartsName(), record.getPurchaseCount()));

    return dao.insertSelective(record);
  }

  /**
   * 查询
   *
   * @param purchaseId
   * @return
   */
  @Override
  public Purchase selectByPrimaryKey(Integer purchaseId) {
    return dao.selectByPrimaryKey(purchaseId);
  }

  /**
   * 可选择更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKeySelective(Purchase record) {
    return dao.updateByPrimaryKeySelective(record);
  }

  /**
   * 更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKey(Purchase record) {
    return dao.updateByPrimaryKey(record);
  }
}
