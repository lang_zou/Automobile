package com.zou.service.impl;

import com.zou.bean.CarInfo;
import com.zou.dao.CarInfoDao;
import com.zou.service.CarInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CarInfoServiceImpl implements CarInfoService {

  // 创建dao层对象
  @Autowired private CarInfoDao dao;

  /**
   * 查询指定车牌号的汽车Id
   *
   * @param carNumber
   */
  @Override
  public int selectIdByNum(String carNumber) {
    return dao.selectIdByNum(carNumber);
  }

  /** 查询所有汽车信息（包括客户信息） */
  @Override
  public List<CarInfo> selectAll() {
    return dao.selectAll();
  }

  /**
   * 删除
   *
   * @param carId
   * @return
   */
  @Override
  public int deleteByPrimaryKey(Integer carId) {
    return dao.deleteByPrimaryKey(carId);
  }

  /**
   * 插入
   *
   * @param record
   * @return
   */
  @Override
  public int insert(CarInfo record) {
    return dao.insert(record);
  }

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  @Override
  public int insertSelective(CarInfo record) {
    return dao.insertSelective(record);
  }

  /**
   * 根据主键查询数据
   *
   * @param carId
   * @return
   */
  @Override
  public CarInfo selectByPrimaryKey(Integer carId) {
    return dao.selectByPrimaryKey(carId);
  }

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKeySelective(CarInfo record) {
    return dao.updateByPrimaryKeySelective(record);
  }

  /**
   * 更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKey(CarInfo record) {
    return dao.updateByPrimaryKey(record);
  }
}
