package com.zou.service.impl;

import com.zou.bean.Accendant;
import com.zou.dao.AccendantDao;
import com.zou.service.AccendantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AccendantServiceImpl implements AccendantService {

  // 创建持久层对象
  @Autowired private AccendantDao dao;

  /**
   * 删除
   *
   * @param accendantId
   * @return
   */
  @Override
  public int deleteByPrimaryKey(Integer accendantId) {
    return dao.deleteByPrimaryKey(accendantId);
  }

  /**
   * 插入
   *
   * @param record
   * @return
   */
  @Override
  public int insert(Accendant record) {
    return dao.insert(record);
  }

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  @Override
  public int insertSelective(Accendant record) {
    return dao.insertSelective(record);
  }

  /**
   * 查询
   *
   * @param accendantId
   * @return
   */
  @Override
  public Accendant selectByPrimaryKey(Integer accendantId) {
    return dao.selectByPrimaryKey(accendantId);
  }

  /**
   * 查询所有
   *
   * @return
   */
  @Override
  public List<Accendant> selectAll() {
    return dao.selectAll();
  }

  /**
   * 更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKeySelective(Accendant record) {
    return dao.updateByPrimaryKeySelective(record);
  }

  /**
   * 更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKey(Accendant record) {
    return dao.updateByPrimaryKey(record);
  }
}
