package com.zou.service.impl;

import com.zou.bean.MainTain;
import com.zou.dao.MainTainDao;
import com.zou.service.MaintainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MaintainServiceImpl implements MaintainService {

  // 创建Dao层对象
  @Autowired private MainTainDao dao;

  /**
   * 查询指定维修ID的客户ID
   *
   * @param maintainId
   */
  @Override
  public int selectClientId(int maintainId) {
    return dao.selectClientId(maintainId);
  }

  /** 查询所有待出库的工单 */
  @Override
  public List<MainTain> selectAllOut() {
    return dao.selectAllOut();
  }

  /** 查询所有已完成工单（多表） */
  @Override
  public List<MainTain> selectAllHis() {
    return dao.selectAllHis();
  }

  /**
   * 完成维修
   *
   * @param id
   */
  @Override
  public int finish(int id) {
    return dao.finish(id);
  }

  /**
   * 取消工单
   *
   * @param id
   */
  @Override
  public int cancel(Integer id) {
    return dao.cancel(id);
  }

  /** 查询所有信息（多表） */
  @Override
  public List<MainTain> selectAll() {
    return dao.selectAll();
  }

  /**
   * 删除
   *
   * @param id
   * @return
   */
  @Override
  public int deleteByPrimaryKey(Integer id) {
    return dao.deleteByPrimaryKey(id);
  }

  /**
   * 插入
   *
   * @param record
   * @return
   */
  @Override
  public int insert(MainTain record) {
    return dao.insert(record);
  }

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  @Override
  public int insertSelective(MainTain record) {
    record.setMaintainNo((Integer.parseInt(dao.selectNo()) + 1) + "");
    dao.insertSelective(record);
    return dao.selectId();
  }

  /**
   * 查询
   *
   * @param id
   * @return
   */
  @Override
  public MainTain selectByPrimaryKey(Integer id) {
    return dao.selectByPrimaryKey(id);
  }

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKeySelective(MainTain record) {
    return dao.updateByPrimaryKeySelective(record);
  }

  /**
   * 更新
   *
   * @param record
   * @return
   */
  @Override
  public int updateByPrimaryKey(MainTain record) {
    return dao.updateByPrimaryKey(record);
  }
}
