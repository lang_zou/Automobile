package com.zou.service;

import com.zou.bean.Purchase;

import java.util.List;

public interface PurchaseService {

  /** 查询最大的采购编号 */
  int selectId();

  /** 查询所有的配件信息 */
  List<Purchase> selectAllParts();

  /** 查询指定类型的配件信息 */
  List<Purchase> selectByType(String partsType);

  /** 查询所有 */
  List<Purchase> selectAll();

  /**
   * 删除
   *
   * @param purchaseId
   * @return
   */
  int deleteByPrimaryKey(Integer purchaseId);

  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(Purchase record);

  /**
   * 可选择插入
   *
   * @param record
   * @return
   */
  int insertSelective(Purchase record);

  /**
   * 查询
   *
   * @param purchaseId
   * @return
   */
  Purchase selectByPrimaryKey(Integer purchaseId);

  /**
   * 可选择更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(Purchase record);

  /**
   * 更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKey(Purchase record);
}
