package com.zou.service;

import com.zou.bean.Resources;

import java.util.List;

public interface ResourceService {

  /** 查询对应用户的所有资源 */
  List<Resources> selectResByUser(int userId);
  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(Resources record);

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  int insertSelective(Resources record);

  /**
   * 删除
   *
   * @param resId
   * @return
   */
  int deleteByPrimaryKey(Integer resId);

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(Resources record);

  /**
   * 查询所有
   *
   * @return
   */
  List<Resources> selectAll();
}
