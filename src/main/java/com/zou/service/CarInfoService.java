package com.zou.service;

import com.zou.bean.CarInfo;

import java.util.List;

public interface CarInfoService {

  /** 查询指定车牌号的汽车Id */
  int selectIdByNum(String carNumber);

  /** 查询所有汽车信息（包括客户信息） */
  List<CarInfo> selectAll();

  /**
   * 删除
   *
   * @param carId
   * @return
   */
  int deleteByPrimaryKey(Integer carId);

  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(CarInfo record);

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  int insertSelective(CarInfo record);

  /**
   * 根据主键查询数据
   *
   * @param carId
   * @return
   */
  CarInfo selectByPrimaryKey(Integer carId);

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(CarInfo record);

  /**
   * 更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKey(CarInfo record);
}
