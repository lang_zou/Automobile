package com.zou.service;

import com.zou.bean.MainTain;

import java.util.List;

public interface MaintainService {

  /** 查询指定维修ID的客户ID */
  int selectClientId(int maintainId);

  /** 查询所有待出库的工单 */
  List<MainTain> selectAllOut();

  /** 查询所有已完成工单（多表） */
  List<MainTain> selectAllHis();

  /** 完成维修 */
  int finish(int id);

  /** 取消工单 */
  int cancel(Integer id);

  /** 查询所有信息（多表） */
  List<MainTain> selectAll();

  /**
   * 删除
   *
   * @param id
   * @return
   */
  int deleteByPrimaryKey(Integer id);

  /**
   * 插入
   *
   * @param record
   * @return
   */
  int insert(MainTain record);

  /**
   * 可选择性插入
   *
   * @param record
   * @return
   */
  int insertSelective(MainTain record);

  /**
   * 查询
   *
   * @param id
   * @return
   */
  MainTain selectByPrimaryKey(Integer id);

  /**
   * 可选择性更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(MainTain record);

  /**
   * 更新
   *
   * @param record
   * @return
   */
  int updateByPrimaryKey(MainTain record);
}
