import org.junit.Test;

/** 测试拼接菜单 */
public class GenMenu {
  @Test
  public void genMenu() {
    StringBuilder str =
        new StringBuilder(
            "[\n"
                + "                {\n"
                + "                \"name\": \"\",\n"
                + "                \"icon\": \"&#xe68e;\",\n"
                + "                \"url\": \"\",\n"
                + "                \"hidden\": true,\n"
                + "                \"list\": [{\n"
                + "                    \"name\": \"首页\",\n"
                + "                    \"url\": \"${pageContext.request.contextPath}/main\",\n"
                + "                }]\n"
                + "            }");

    str.append(
        " , {\n"
            + "                    \"name\": \"退出登录\",\n"
            + "                    \"icon\": \"&#xe65c;\",\n"
            + "                    \"url\": \"${pageContext.request.contextPath}/out\",\n"
            + "                    \"list\": []\n"
            + "                }\n"
            + "            ]");

    System.out.println(str);
  }
}
