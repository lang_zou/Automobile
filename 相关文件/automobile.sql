/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 5.5.62 : Database - automobile
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`automobile` /*!40100 DEFAULT CHARACTER SET gb2312 */;

USE `automobile`;

/*Table structure for table `accendant` */

DROP TABLE IF EXISTS `accendant`;

CREATE TABLE `accendant` (
  `accendantId` int(11) NOT NULL AUTO_INCREMENT,
  `accendantName` varchar(20) DEFAULT NULL,
  `phone` char(11) DEFAULT NULL,
  PRIMARY KEY (`accendantId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=gb2312;

/*Data for the table `accendant` */

insert  into `accendant`(`accendantId`,`accendantName`,`phone`) values (2,'张三','13933333333'),(3,'李四','10044444444'),(5,'王五','12555555555'),(6,'刘六','16666666666'),(7,'柒柒','12777777777');

/*Table structure for table `carinfo` */

DROP TABLE IF EXISTS `carinfo`;

CREATE TABLE `carinfo` (
  `carId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `carBrand` varchar(20) CHARACTER SET gb2312 DEFAULT NULL,
  `carNumber` varchar(20) CHARACTER SET gb2312 DEFAULT NULL,
  PRIMARY KEY (`carId`),
  UNIQUE KEY `carinfo_carNumber_uindex` (`carNumber`),
  KEY `clientId` (`clientId`),
  CONSTRAINT `carinfo_ibfk_1` FOREIGN KEY (`clientId`) REFERENCES `clientinfo` (`clientId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/*Data for the table `carinfo` */

insert  into `carinfo`(`carId`,`clientId`,`carBrand`,`carNumber`) values (13,1,'奥迪 A4L','赣C99999'),(14,1,'兰博基尼','赣CW6666'),(15,2,'保时捷 帕拉梅拉','鲁R66666'),(16,3,'宝马 740','鲁R88888'),(29,14,'兰博基尼','赣C43242'),(30,8,'阿斯顿 马丁 DBS','沪A66666'),(31,2,'奔驰 大G500','鲁R99009'),(32,3,'凯迪拉克','赣A23132');

/*Table structure for table `clientinfo` */

DROP TABLE IF EXISTS `clientinfo`;

CREATE TABLE `clientinfo` (
  `clientId` int(11) NOT NULL AUTO_INCREMENT,
  `clientName` varchar(25) DEFAULT NULL,
  `sex` char(4) DEFAULT '男',
  `phone` char(11) DEFAULT NULL,
  `createDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`clientId`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=gb2312;

/*Data for the table `clientinfo` */

insert  into `clientinfo`(`clientId`,`clientName`,`sex`,`phone`,`createDate`) values (1,'黄周阳','男','13689891212','2021-04-20 18:13:04'),(2,'王建辉','男','15978783434','2021-04-20 18:13:32'),(3,'余喜民','男','18979792525','2021-04-20 18:14:35'),(4,'蔡紫林','男','17978783232','2021-04-21 21:28:00'),(8,'邹仁波','男','18979961517','2021-04-22 14:08:41'),(10,'武乐萍','男','15789893243','2021-04-22 14:14:56'),(14,'梁松','男','16754549898','2021-04-22 17:46:39'),(21,'迪迦','男','345235315','2021-04-28 08:03:00'),(22,'赛文','男','21423414152','2021-04-28 08:03:34'),(23,'赛罗','男','13643322434','2021-04-28 08:05:43'),(24,'基多拉','女','17623421231','2021-04-28 08:06:13'),(25,'亚索','男','13643622434','2021-04-28 08:06:29'),(26,'阿卡丽','女','13621342123','2021-04-28 08:06:44'),(27,'腕豪','男','17634535233','2021-04-28 08:07:14'),(28,'宇健租车公司','男','15634214332','2021-04-29 09:41:21');

/*Table structure for table `fund` */

DROP TABLE IF EXISTS `fund`;

CREATE TABLE `fund` (
  `fundId` int(11) NOT NULL AUTO_INCREMENT,
  `fundType` varchar(20) DEFAULT NULL,
  `fundNo` char(6) DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  PRIMARY KEY (`fundId`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

/*Data for the table `fund` */

/*Table structure for table `maintain` */

DROP TABLE IF EXISTS `maintain`;

CREATE TABLE `maintain` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `maintainNo` int(11) DEFAULT NULL,
  `carId` int(11) DEFAULT NULL,
  `maintainDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `finishDate` timestamp NULL DEFAULT NULL,
  `maintainInfo` varchar(100) DEFAULT NULL,
  `maintainCost` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `accendantId` int(11) DEFAULT NULL,
  `isEnable` int(11) NOT NULL DEFAULT '1',
  `isDelete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`,`isEnable`,`isDelete`),
  UNIQUE KEY `maintainNo` (`maintainNo`),
  KEY `maintain_ibfk_2` (`accendantId`),
  KEY `carId` (`carId`),
  CONSTRAINT `maintain_ibfk_2` FOREIGN KEY (`accendantId`) REFERENCES `accendant` (`accendantId`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `maintain_ibfk_4` FOREIGN KEY (`carId`) REFERENCES `carinfo` (`carId`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=gb2312;

/*Data for the table `maintain` */

insert  into `maintain`(`Id`,`maintainNo`,`carId`,`maintainDate`,`finishDate`,`maintainInfo`,`maintainCost`,`state`,`accendantId`,`isEnable`,`isDelete`) values (1,1,13,'2021-04-23 09:46:39','2021-04-27 18:22:51','无',667,1,2,1,0),(2,2,15,'2021-04-23 09:55:48','2021-04-25 21:42:15','无',888,0,3,0,0),(3,3,16,'2021-04-23 15:14:21','2021-04-25 21:19:05','无',777,0,3,0,0),(5,4,14,'2021-04-27 18:06:35',NULL,'无',123,0,3,0,0),(6,5,29,'2021-04-27 18:44:25','2021-04-28 08:11:42','无',123,1,7,1,0),(7,6,15,'2021-04-27 18:47:29','2021-04-29 11:12:18','无',363,1,6,1,0),(8,7,13,'2021-04-27 20:42:55','2021-04-27 21:16:35','无',133,1,7,1,0),(9,8,14,'2021-04-27 20:46:14','2021-04-29 11:12:44','无',124,1,5,1,0),(10,9,15,'2021-04-27 21:17:50',NULL,'无',345,2,6,0,0),(11,10,15,'2021-04-27 21:18:06','2021-04-29 11:13:22','无',342,1,6,1,0),(16,11,14,'2021-04-27 21:22:59',NULL,'无',23,0,6,0,0),(17,12,29,'2021-04-27 21:35:43',NULL,'无',1123,0,6,0,0),(18,13,16,'2021-04-27 21:42:55','2021-04-28 07:22:45','发功机坏了',123,1,5,1,0),(19,14,14,'2021-04-27 21:43:16',NULL,'无',234,0,5,0,0),(20,15,14,'2021-04-28 07:40:57','2021-04-29 11:06:29','无',123,1,6,1,0),(21,16,15,'2021-04-28 07:44:40','2021-04-29 10:40:17','无',1223,1,3,1,0),(22,17,31,'2021-04-28 08:10:07','2021-04-29 11:06:12','无',123,1,5,1,0),(23,18,14,'2021-04-28 08:10:18',NULL,'无',123,2,6,0,0),(24,19,14,'2021-04-29 09:45:16',NULL,'无',564,2,6,0,0),(28,21,15,'2021-04-29 11:14:11','2021-04-29 11:14:26','无',323,1,6,1,0),(29,22,15,'2021-04-29 11:16:10','2021-04-29 11:16:14','刹车有问题',345,1,6,1,0),(30,23,14,'2021-04-29 11:25:00','2021-04-29 11:25:04','无',45,1,6,1,0),(31,24,NULL,'2021-04-29 15:29:14',NULL,'这是宇健租车公司的车，请认真对待！',NULL,2,7,0,0),(32,25,NULL,'2021-04-29 15:42:10','2021-04-29 15:50:32','这是宇健租车公司的车，请认真对待！',546,1,7,1,0),(33,26,16,'2021-04-29 15:43:09','2021-04-29 15:43:15','无',234,1,6,1,0),(34,27,NULL,'2021-04-29 15:53:22','2021-04-29 15:54:20','这是宇健租车公司的车，请认真对待！',123456,1,7,1,0),(35,28,NULL,'2021-04-29 15:55:39','2021-04-29 15:56:08','这是宇健234租车公司的车，请认真对待！',123,1,7,1,0),(36,29,NULL,'2021-04-29 17:20:11',NULL,'这是宇健租车公司的车，请认真对待！',NULL,0,7,0,0),(37,30,NULL,'2021-04-30 10:20:00',NULL,'这是宇健租车公司的车，请认真对待！',NULL,0,7,0,0),(38,31,NULL,'2021-04-30 10:20:34','2021-04-30 10:23:15','这是宇健租车公司的车，请认真对待！',324,1,7,1,0),(39,32,NULL,'2021-04-30 10:26:17','2021-04-30 10:28:13','这是宇健租车公司的车，请认真对待！',678,1,7,1,0),(40,33,NULL,'2021-04-30 10:32:36','2021-04-30 10:35:00','这是宇健租车公司的车，请认真对待！',432,1,7,1,0),(41,34,NULL,'2021-04-30 10:36:51','2021-04-30 10:37:06','这是宇健租车公司的车，请认真对待！',235,1,7,1,0),(42,35,NULL,'2021-04-30 10:43:39','2021-04-30 10:44:10','这是宇健租车公司的车，请认真对待！',657,1,7,1,0),(43,36,NULL,'2021-04-30 10:47:05',NULL,'这是宇健租车公司的车，请认真对待！',NULL,0,7,1,0),(44,37,NULL,'2021-04-30 10:50:01',NULL,'这是宇健租车公司的车，请认真对待！',NULL,2,7,0,0),(45,38,NULL,'2021-04-30 10:58:28','2021-04-30 11:01:24','这是宇健租车公司的车，请认真对待！',657,1,7,1,0),(46,39,NULL,'2021-04-30 11:03:18','2021-04-30 11:04:44','这是宇健租车公司的车，请认真对待！',34,1,7,1,0),(47,40,NULL,'2021-04-30 11:06:23','2021-04-30 11:08:42','这是宇健租车公司的车，请认真对待！',67,1,7,1,0),(48,41,13,'2021-04-30 11:10:27','2021-04-30 11:10:32','无',789,1,5,1,0),(49,42,NULL,'2021-04-30 11:13:14','2021-04-30 11:13:33','这是宇健租车公司的车，请认真对待！',7668,1,7,1,0),(50,43,NULL,'2021-04-30 11:14:08',NULL,'这是宇健租车公司的车，请认真对待！',NULL,0,7,1,0),(51,44,NULL,'2021-04-30 11:26:18',NULL,'这是宇健租车公司的车，请认真对待！',NULL,0,7,1,0),(52,45,31,'2021-05-05 18:25:59','2021-05-05 18:27:15','方向制动有问题',500,1,5,1,0),(53,46,29,'2021-05-06 08:47:34',NULL,'无',531,0,6,1,0),(55,47,29,'2021-05-06 14:40:24','2021-05-06 14:41:22','……',124,1,5,1,0);

/*Table structure for table `partsinfo` */

DROP TABLE IF EXISTS `partsinfo`;

CREATE TABLE `partsinfo` (
  `partsInfoId` int(11) NOT NULL AUTO_INCREMENT,
  `partsName` varchar(30) DEFAULT NULL,
  `partsType` varchar(20) DEFAULT NULL,
  `purPrice` int(11) DEFAULT NULL,
  PRIMARY KEY (`partsInfoId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=gb2312;

/*Data for the table `partsinfo` */

insert  into `partsinfo`(`partsInfoId`,`partsName`,`partsType`,`purPrice`) values (1,'缸盖','发动机配件',888),(2,'油底壳','发动机配件',222),(3,'机体','发动机配件',567),(4,'活塞','发动机配件',678),(5,'飞轮','传动系配件',123),(6,'压盘','传动系配件',478),(7,'离合器片','传动系配件',333),(8,'变速器','传动系配件',444),(9,'制动泵','制动系配件',555),(10,'真空助力器','制动系配件',324),(11,'制动踏板总成','制动系配件',78),(12,'方向盘','转向系配件',66),(13,'转向节','转向系配件',128),(14,'转向拉杆','转向系配件',78),(15,'排气管','发动机配件',105),(16,'传动轴','传动系配件',225),(17,'刹车片','制动系配件',35);

/*Table structure for table `partsrepertory` */

DROP TABLE IF EXISTS `partsrepertory`;

CREATE TABLE `partsrepertory` (
  `partsId` int(11) NOT NULL AUTO_INCREMENT,
  `partsName` varchar(25) DEFAULT NULL,
  `partsType` varchar(25) DEFAULT NULL,
  `partsPrice` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT '10',
  PRIMARY KEY (`partsId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=gb2312;

/*Data for the table `partsrepertory` */

insert  into `partsrepertory`(`partsId`,`partsName`,`partsType`,`partsPrice`,`count`) values (1,'缸盖','发动机配件',88,14),(2,'油底壳','发动机配件',225,11),(3,'机体','发动机配件',566,-2),(7,'活塞','发动机配件',68,33),(8,'飞轮','传动系配件',123,6),(9,'压盘','传动系配件',478,6),(10,'离合器片','传动系配件',333,7),(11,'变速器','传动系配件',444,7),(12,'制动泵','制动系配件',555,7),(13,'真空助力器','制动系配件',324,7),(14,'制动踏板总成','制动系配件',78,8),(15,'方向盘','转向系配件',66,7),(16,'转向节','转向系配件',128,9),(17,'转向拉杆','转向系配件',78,10),(18,'排气管','发动机配件',105,-2),(19,'传动轴','传动系配件',225,7),(20,'刹车片','制动系配件',35,7);

/*Table structure for table `partsuse` */

DROP TABLE IF EXISTS `partsuse`;

CREATE TABLE `partsuse` (
  `useId` int(11) NOT NULL AUTO_INCREMENT,
  `maintainId` int(11) DEFAULT NULL,
  `partsId` int(11) DEFAULT NULL,
  PRIMARY KEY (`useId`),
  KEY `maintainId` (`maintainId`),
  KEY `partsId` (`partsId`),
  CONSTRAINT `partsuse_ibfk_1` FOREIGN KEY (`maintainId`) REFERENCES `maintain` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `partsuse_ibfk_2` FOREIGN KEY (`partsId`) REFERENCES `partsrepertory` (`partsId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=gb2312;

/*Data for the table `partsuse` */

insert  into `partsuse`(`useId`,`maintainId`,`partsId`) values (1,1,1),(2,1,2),(3,2,3),(6,5,8),(7,5,9),(8,5,10),(9,5,11),(10,5,19),(11,6,12),(12,6,13),(13,6,14),(14,6,20),(15,7,12),(16,7,13),(17,7,14),(18,7,20),(19,8,8),(20,8,9),(21,8,10),(22,8,11),(23,8,19),(24,9,8),(25,9,9),(26,9,10),(27,9,11),(28,9,19),(29,10,12),(30,10,13),(31,10,14),(32,10,20),(33,11,12),(34,11,13),(35,11,14),(36,11,20),(37,16,12),(38,16,13),(39,16,14),(40,16,20),(41,17,1),(42,17,2),(43,17,3),(44,17,7),(45,17,18),(46,19,12),(47,20,15),(48,21,15),(49,23,15),(50,23,16),(51,23,17),(52,24,3),(53,24,7),(54,24,18),(64,28,8),(65,28,9),(66,28,10),(67,28,11),(68,28,19),(69,31,3),(70,31,7),(71,31,18),(72,32,3),(73,32,7),(74,32,18),(75,34,3),(76,34,7),(77,34,18),(78,35,3),(79,35,7),(80,35,18),(81,36,3),(82,36,7),(83,36,18),(84,37,3),(85,37,7),(86,37,18),(87,38,3),(88,38,7),(89,38,18),(90,39,3),(91,39,7),(92,39,18),(93,40,3),(94,40,7),(95,40,18),(96,41,3),(97,41,7),(98,41,18),(99,42,3),(100,42,7),(101,42,18),(102,43,3),(103,43,7),(104,43,18),(105,44,3),(106,44,7),(107,44,18),(108,45,3),(109,45,7),(110,45,18),(111,46,3),(112,46,7),(113,46,18),(114,47,3),(115,47,7),(116,47,18),(117,49,3),(118,49,7),(119,49,18),(120,50,3),(121,50,7),(122,50,18),(123,51,3),(124,51,7),(125,51,18),(126,52,8),(127,52,9),(128,53,3),(129,53,7),(130,55,15),(131,55,16);

/*Table structure for table `privliege` */

DROP TABLE IF EXISTS `privliege`;

CREATE TABLE `privliege` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  KEY `priId` (`Id`),
  KEY `fk_privliege_roleInfo_roleId` (`roleId`),
  KEY `fk_privliege_userInfo_userId` (`userId`),
  CONSTRAINT `fk_privliege_roleInfo_roleId` FOREIGN KEY (`roleId`) REFERENCES `roleinfo` (`roleId`),
  CONSTRAINT `fk_privliege_userInfo_userId` FOREIGN KEY (`userId`) REFERENCES `userinfo` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `privliege` */

insert  into `privliege`(`Id`,`userId`,`roleId`) values (1,1,1),(2,1,2),(3,1,4),(4,2,1),(5,2,2),(6,1,3),(7,2,3),(8,4,1),(9,6,2),(10,7,3),(11,8,1),(12,8,2),(13,9,1);

/*Table structure for table `purchase` */

DROP TABLE IF EXISTS `purchase`;

CREATE TABLE `purchase` (
  `purchaseId` int(11) NOT NULL AUTO_INCREMENT,
  `purchaseNo` int(10) NOT NULL,
  `partsInfoId` int(11) DEFAULT NULL,
  `purchaseCount` int(11) DEFAULT NULL,
  `purchaseDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `purchaseSumMoney` int(11) DEFAULT NULL,
  PRIMARY KEY (`purchaseId`),
  KEY `partsInfoId` (`partsInfoId`),
  CONSTRAINT `purchase_ibfk_1` FOREIGN KEY (`partsInfoId`) REFERENCES `partsinfo` (`partsInfoId`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=gb2312;

/*Data for the table `purchase` */

insert  into `purchase`(`purchaseId`,`purchaseNo`,`partsInfoId`,`purchaseCount`,`purchaseDate`,`purchaseSumMoney`) values (1,1,1,10,'2021-04-26 18:52:44',8880),(2,2,1,2,'2021-04-27 10:10:07',1776),(3,3,1,3,'2021-04-27 11:04:34',2664),(5,4,2,2,'2021-04-27 11:12:58',444),(6,5,11,1,'2021-04-30 07:42:53',78),(7,6,4,12,'2021-05-06 08:52:05',8136),(8,7,4,23,'2021-05-06 14:42:05',15594);

/*Table structure for table `resources` */

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources` (
  `resId` int(11) NOT NULL AUTO_INCREMENT,
  `resName` varchar(20) NOT NULL,
  `resUrl` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `isEnable` int(11) DEFAULT '1',
  KEY `resId` (`resId`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

/*Data for the table `resources` */

insert  into `resources`(`resId`,`resName`,`resUrl`,`level`,`parentId`,`isEnable`) values (9,'配件管理','',1,NULL,1),(18,'配件采购','parts-buy',2,9,1),(19,'配件出库','parts-use',2,9,1),(20,'库存管理','parts-manage',2,9,1),(21,'维修接待',NULL,1,NULL,1),(22,'工单处理','order-manage',2,21,1),(23,'历史查询','order-list',2,21,1),(24,'预约管理','sub-manage',2,21,1),(25,'汽车健康档案','car-list',2,21,1),(26,'配件基础数据','parts-list',2,21,1),(27,'客户关系',NULL,1,NULL,1),(28,'客户资料','client-list',2,27,1),(29,'业务统计','order-list',2,27,1),(30,'客户分析','client-pro',2,27,1),(31,'系统配置',NULL,1,NULL,1),(32,'用户管理','user-list',2,31,1),(33,'角色管理','role-list',2,31,1),(34,'资源管理','res-list',2,31,1);

/*Table structure for table `roleinfo` */

DROP TABLE IF EXISTS `roleinfo`;

CREATE TABLE `roleinfo` (
  `roleId` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(20) DEFAULT NULL,
  `roleDesc` varchar(50) DEFAULT NULL,
  `isEnable` int(11) DEFAULT '1',
  KEY `roleId` (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `roleinfo` */

insert  into `roleinfo`(`roleId`,`roleName`,`roleDesc`,`isEnable`) values (1,'仓库管理员','管理配件',1),(2,'前台接待','接待客户',1),(3,'财务人员','管理公司收支',1),(4,'管理员','权限管理',1);

/*Table structure for table `roleresource` */

DROP TABLE IF EXISTS `roleresource`;

CREATE TABLE `roleresource` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `roleId` int(11) DEFAULT NULL,
  `resId` int(11) DEFAULT NULL,
  KEY `rolResId` (`Id`),
  KEY `fk_roleResource_roleInfo_roleId` (`roleId`),
  KEY `fk_roleResource_resources_resId` (`resId`),
  CONSTRAINT `fk_roleResource_resources_resId` FOREIGN KEY (`resId`) REFERENCES `resources` (`resId`),
  CONSTRAINT `fk_roleResource_roleInfo_roleId` FOREIGN KEY (`roleId`) REFERENCES `roleinfo` (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `roleresource` */

insert  into `roleresource`(`Id`,`roleId`,`resId`) values (1,1,9),(2,1,18),(3,1,19),(4,2,21),(5,2,22),(6,2,23),(7,2,24),(8,2,25),(9,2,26),(10,2,27),(11,2,28),(12,2,29),(13,2,30),(14,4,31),(15,4,32),(16,4,33),(17,4,34);

/*Table structure for table `userinfo` */

DROP TABLE IF EXISTS `userinfo`;

CREATE TABLE `userinfo` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) NOT NULL,
  `loginName` varchar(20) DEFAULT NULL,
  `userPwd` varchar(50) NOT NULL,
  `createDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `loginState` int(11) DEFAULT '0',
  `isDelete` int(11) DEFAULT '0',
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `userinfo` */

insert  into `userinfo`(`userId`,`userName`,`loginName`,`userPwd`,`createDate`,`loginState`,`isDelete`) values (1,'邹仁波','a001','-7e236424ad2fb23dffc92427cec12fab','2021-04-15 14:22:06',0,0),(2,'王建辉','s001','-7e236424ad2fb23dffc92427cec12fab','2021-04-15 14:23:34',0,0),(4,'黄周阳','s002','-7e236424ad2fb23dffc92427cec12fab','2021-04-25 07:52:43',0,0),(6,'李龙杰','Q001','-7e236424ad2fb23dffc92427cec12fab','2021-04-28 10:19:46',0,0),(7,'余喜民','Y001','-16aeff7e53cf0057c0ef497321e353f9','2021-04-29 08:45:15',0,0),(8,'张宇健','Z001','-7e236424ad2fb23dffc92427cec12fab','2021-04-29 09:56:58',0,0),(9,'张三','c001','-7e236424ad2fb23dffc92427cec12fab','2021-05-06 08:24:15',0,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
